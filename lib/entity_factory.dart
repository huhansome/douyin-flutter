import 'package:myproject/bean/Chat_entity.dart';
import 'package:myproject/bean/User_entity.dart';
import 'package:myproject/bean/Aweme_entity.dart';

class EntityFactory {
  static T generateOBJ<T>(json) {
    if (1 == 0) {
      return null;
    } else if (T.toString() == "AwemeEntity") {
      return AwemeEntity.fromJson(json) as T;
    } else if (T.toString() == "ChatEntity") {
      return ChatEntity.fromJson(json) as T;
    } else if (T.toString() == "UserEntity") {
      return UserEntity.fromJson(json) as T;
    } else {
      return null;
    }
  }
}