import 'package:flutter/material.dart';

class Const {
  static const tabbarAssets = 'assets/images/tabbar/';
  static const iconAssets = 'assets/images/icons/';
  static const emotionAssets = 'assets/images/emotions/';

  static const themeColor = Colors.black87;

}