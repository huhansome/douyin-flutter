import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:myproject/bean/Chat_entity.dart';
import 'dart:convert';
import 'package:myproject/util/Const.dart';
import 'package:myproject/pages/GridViewWithHeader.dart';

class MessagePage extends StatefulWidget {
  @override
  _MessagePageState createState() => _MessagePageState();
}

class _MessagePageState extends State<MessagePage> {

  List<ChatEntity> list = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    readLocalJson().then((value) {
      for(var dic in value['data']) {
        ChatEntity chat = ChatEntity.fromJson(dic);
        list.add(chat);
      }
      setState(() {
        list = list;
      });
    });
  }

  Future<dynamic> readLocalJson() async {
    var responseStr = await rootBundle.loadString('mock/groupchats.json');
    var responseJson = json.decode(responseStr);
//    print('==============='+responseStr);
    return responseJson;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Const.themeColor,
        shadowColor: Const.themeColor,
        title: const Text('消息'),
        actions: <Widget>[
          InkWell(
              child: Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(right: 15),
                height: 44,
                 child: Text('创建群聊'),
               ),

              onTap: () {
                Fluttertoast.showToast(
                    msg: '创建群聊', gravity: ToastGravity.CENTER);
                Navigator.of(context).push(new MaterialPageRoute(builder: (context) {
                  return GridViewWithHeader();
                }));
              }
          )
        ],
      ),
      body: Container(
        decoration: BoxDecoration(
          color: Const.themeColor,
          border: Border(top: BorderSide(color: Colors.white70, width: 0.2) ),
        ),
        child: ListView.separated(
          itemCount: list.length,
          separatorBuilder: (BuildContext context, int index) => Divider(
            color: Colors.transparent,
//            height: 0.1,
          ),
          itemBuilder: (BuildContext context, int index) {
            return ListTile(
              leading: CircleAvatar(
                  backgroundImage: Image.network(list[index].visitor.avatarThumbnail.url).image
              ),
              title: Text(list[index].visitor.uid, style: TextStyle(color: Colors.white),),
              subtitle: Text(list[index].msgContent, style: TextStyle(color: Colors.white70)),
              trailing: Text(list[index].createTime, style: TextStyle(color: Colors.white70)),
            );
          },
        ),
      )
    );
  }
}




