import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import '../util/Const.dart';
import 'dart:convert';

class MePage1 extends StatefulWidget {
  @override
  _MePageState createState() => _MePageState();
}

class _MePageState extends State<MePage1> with TickerProviderStateMixin {
  AnimationController animationController;
  Animation<double> anim;
  double currentHeight = 130;
  double extraPicHeight = 0;
  BoxFit fitType;
  double prev_dy;

  final List<Tab> myTabs = <Tab>[
    Tab(text: '作品'),
    Tab(text: '动态'),
    Tab(text: '喜欢'),
  ];

  List<String> getDataList() {
    List<String> list = [];
    for (int i = 0; i < 100; i++) {
      list.add(i.toString());
    }
    return list;
  }

  List<Widget> getWidgetList() {
    return getDataList().map((item) => getItemContainer(item)).toList();
  }

  Widget getItemContainer(String item) {
    return Container(
      alignment: Alignment.center,
      child: Text(
        item,
        style: TextStyle(color: Colors.white, fontSize: 20),
      ),
      color: Colors.blue,
    );
  }

  TabController _tabController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    prev_dy = 0;
    fitType = BoxFit.fitWidth;

    animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    anim = Tween(begin: 0.0, end: 0.0).animate(animationController);
    _tabController = TabController(length: myTabs.length, vsync: this);
  }

//  https://www.imooc.com/article/304365
//  https://blog.csdn.net/sinat_17775997/article/details/110536075
//  https://github.com/zhaolongs/flutter_demo_app

  runAnimate() {
    //设置动画让extraPicHeight的值从当前的值渐渐回到 0
    setState(() {
      anim = Tween(begin: extraPicHeight, end: 0.0).animate(animationController)
        ..addListener(() {
          if (extraPicHeight >= 45) {
            //同样改变图片填充类型
            fitType = BoxFit.fitHeight;
          } else {
            fitType = BoxFit.fitWidth;
          }
          setState(() {
            extraPicHeight = anim.value;
            print('----------$extraPicHeight');
            currentHeight = 130 + extraPicHeight;
            fitType = fitType;
          });
        });
      prev_dy = 0; //同样归零
    });
  }

  updatePicHeight(changed) {
    if (prev_dy == 0) {
      //如果是手指第一次点下时，我们不希望图片大小就直接发生变化，所以进行一个判定。
      prev_dy = changed;
    }
    print('========= $changed');
    setState(() {
      //更新数据
      var offset = changed - prev_dy;
      if (extraPicHeight + offset == -130) {
        extraPicHeight = -129.99;
      } else {
        extraPicHeight += offset;
      }

      print('=============$offset');

      if (currentHeight + offset <= 0.01) {
        currentHeight = 0.01;
      } else {
        currentHeight += offset;
      }
      prev_dy = changed;
//      fitType = fitType;
    });
  }

  @override
  Widget build(BuildContext context) {

  }

//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      backgroundColor: Const.themeColor,
//      extendBodyBehindAppBar: true,
//      appBar: AppBar(
//        backgroundColor: Colors.transparent,
//        actions: <Widget>[
//          IconButton(
//              icon: Image.asset(Const.tabbarAssets + 'icon_home_add.png'),
//              onPressed: () {})
//        ],
//      ),
//      body: SingleChildScrollView(
//        child: Container(
//          height: MediaQuery.of(context).size.height,
//          width: MediaQuery.of(context).size.width,
//          child: Column(
//            children: <Widget>[
//              CachedNetworkImage(
//                imageUrl: "https://pb3.pstatp.com/obj/dbc1001cd29ccc479f7f",
//                placeholder: (context, url) => CircularProgressIndicator(),
//                errorWidget: (context, url, error) => Icon(Icons.error),
//                height: currentHeight,
//                width: MediaQuery.of(context).size.width,
//                fit: BoxFit.fill,
//              ),
//              Container(
//                color: Const.themeColor,
////            margin:EdgeInsets.only(top: 10, bottom: 10),
//                height: 240,
//                child: Column(
//                  crossAxisAlignment: CrossAxisAlignment.start,
//                  children: <Widget>[
//                    Row(
//                      children: <Widget>[
//                        ClipRRect(
//                          borderRadius: BorderRadius.circular(40),
//                          child: CachedNetworkImage(
//                            imageUrl:
//                                "https://pb3.pstatp.com/obj/dbc1001cd29ccc479f7f",
//                            placeholder: (context, url) =>
//                                CircularProgressIndicator(),
//                            errorWidget: (context, url, error) =>
//                                Icon(Icons.error),
//                            height: 80,
//                            width: 80,
//                            fit: BoxFit.fill,
//                          ),
//                        ),
//                        Expanded(
//                          child: Row(
//                            mainAxisAlignment: MainAxisAlignment.end,
//                            children: <Widget>[
//                              Container(
//                                margin: EdgeInsets.only(right: 5),
//                                child: ClipRRect(
//                                  borderRadius: BorderRadius.circular(4),
//                                  child: Container(
//                                    height: 40,
//                                    padding:
//                                        EdgeInsets.only(left: 40, right: 40),
//                                    color: Colors.white54,
//                                    child: InkWell(
//                                      child: Center(
//                                        child: Text('编辑资料',
//                                            style: TextStyle(
//                                                color: Colors.white54,
//                                                fontSize: 15)),
//                                      ),
//                                      onTap: () {},
//                                    ),
//                                  ),
//                                ),
//                              ),
//                              Container(
//                                margin: EdgeInsets.only(right: 15),
//                                child: ClipRRect(
//                                  borderRadius: BorderRadius.circular(4),
//                                  child: Container(
//                                    height: 40,
//                                    padding:
//                                        EdgeInsets.only(left: 10, right: 10),
//                                    color: Colors.white54,
//                                    child: InkWell(
//                                      child: Center(
//                                        child: Text('+好友',
//                                            style: TextStyle(
//                                                color: Colors.white54,
//                                                fontSize: 15)),
//                                      ),
//                                      onTap: () {
//                                        print('-----------');
//                                      },
//                                    ),
//                                  ),
//                                ),
//                              ),
//                            ],
//                          ),
//                        ),
//                      ],
//                    ),
//                    Container(
//                      margin: EdgeInsets.only(top: 10, bottom: 10),
//                      child: Text('抖音昵称',
//                          style: TextStyle(color: Colors.white, fontSize: 18)),
//                    ),
//                    Container(
//                      margin: EdgeInsets.only(top: 0, bottom: 5),
//                      child: Text('抖音号:3452345',
//                          style: TextStyle(color: Colors.white, fontSize: 12)),
//                    ),
//                    Container(
//                      margin: EdgeInsets.only(top: 5, bottom: 5),
//                      child: Center(
//                        child: Container(
//                          height: 0.5,
//                          color: Colors.white,
//                          width: MediaQuery.of(context).size.width - 30,
//                        ),
//                      ),
//                    ),
//                    Container(
//                      margin: EdgeInsets.only(top: 0, bottom: 10),
//                      child: Text('抖音账号简介',
//                          style:
//                              TextStyle(color: Colors.white70, fontSize: 15)),
//                    ),
//                    Container(
//                      margin: EdgeInsets.only(top: 0, bottom: 10),
//                      child: Row(
//                        children: <Widget>[
//                          Container(
//                            color: Colors.white38,
//                            margin: EdgeInsets.only(right: 5),
//                            padding: EdgeInsets.only(left: 4, right: 4),
//                            child: InkWell(
//                              child: Text('22岁',
//                                  style: TextStyle(
//                                      color: Colors.white54, fontSize: 12)),
//                              onTap: () {},
//                            ),
//                          ),
//                          Container(
//                            color: Colors.white38,
//                            margin: EdgeInsets.only(right: 5),
//                            padding: EdgeInsets.only(left: 4, right: 4),
//                            child: InkWell(
//                              child: Text('香港',
//                                  style: TextStyle(
//                                      color: Colors.white54, fontSize: 12)),
//                              onTap: () {},
//                            ),
//                          ),
//                          Container(
//                            color: Colors.white38,
//                            margin: EdgeInsets.only(right: 5),
//                            padding: EdgeInsets.only(left: 4, right: 4),
//                            child: InkWell(
//                              child: Text('增加学校等标签',
//                                  style: TextStyle(
//                                      color: Colors.white54, fontSize: 12)),
//                              onTap: () {},
//                            ),
//                          ),
//                        ],
//                      ),
//                    ),
//                    Container(
//                      child: Row(children: <Widget>[
//                        Container(
//                          margin: EdgeInsets.only(right: 5),
//                          child: InkWell(
//                            child: Row(
//                              children: <Widget>[
//                                Text('99w',
//                                    style: TextStyle(
//                                        color: Colors.white, fontSize: 15)),
//                                Text('获赞',
//                                    style: TextStyle(
//                                        color: Colors.white54, fontSize: 15)),
//                              ],
//                            ),
//                            onTap: () {},
//                          ),
//                        ),
//                        Container(
//                          margin: EdgeInsets.only(right: 5),
//                          child: InkWell(
//                            child: Row(
//                              children: <Widget>[
//                                Text('999w',
//                                    style: TextStyle(
//                                        color: Colors.white, fontSize: 15)),
//                                Text('关注',
//                                    style: TextStyle(
//                                        color: Colors.white54, fontSize: 15)),
//                              ],
//                            ),
//                            onTap: () {},
//                          ),
//                        ),
//                        Container(
//                          margin: EdgeInsets.only(right: 5),
//                          child: InkWell(
//                            child: Row(
//                              children: <Widget>[
//                                Text('10w',
//                                    style: TextStyle(
//                                        color: Colors.white, fontSize: 15)),
//                                Text('粉丝',
//                                    style: TextStyle(
//                                        color: Colors.white54, fontSize: 15)),
//                              ],
//                            ),
//                            onTap: () {},
//                          ),
//                        ),
//                      ]),
//                    ),
//                  ],
//                ),
//              ),
//              Expanded(
//                child: Container(
//                    color: Const.themeColor,
//                    child: Column(
//                      children: <Widget>[
//                        TabBar(
//                          labelColor: Colors.white,
//                          controller: _tabController,
//                          tabs: myTabs,
//                          indicatorColor: Colors.yellow,
//                        ),
//                        Expanded(
//                          child: TabBarView(
//                              physics: BouncingScrollPhysics(),
//                              controller: _tabController,
//                              children: <Widget>[
//                                GridView.count(
//                                  //水平子Widget之间间距
//                                  crossAxisSpacing: 1.0,
//                                  //垂直子Widget之间间距
//                                  mainAxisSpacing: 1.0,
//                                  //GridView内边距
//                                  padding: EdgeInsets.all(0),
//                                  //一行的Widget数量
//                                  crossAxisCount: 3,
//                                  //子Widget宽高比例
//                                  childAspectRatio: 0.5624,
//                                  //子Widget列表
//                                  children: getWidgetList(),
//                                ),
//                                Center(
//                                  child: Text(
//                                    '+++++++',
//                                    style: TextStyle(color: Colors.white),
//                                  ),
//                                ),
//                                Center(
//                                  child: Text('xxxxxxx',
//                                      style: TextStyle(color: Colors.white)),
//                                ),
//                              ]),
//                        )
//                      ],
//                    )),
//              ),
//            ],
//          ),
//        ),
//      ),
//    );
//  }
}
