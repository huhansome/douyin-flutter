import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import '../util/Const.dart';
import '../util/ToastUtil.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import '../bean/User_entity.dart';
import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;
import 'package:myproject/bean/Aweme_entity.dart';
import 'PlayPage.dart';
import 'package:myproject/pages/UserProfilePage.dart';


class MePage extends StatefulWidget {
  @override
  _MePageState createState() => _MePageState();
}

class _MePageState extends State<MePage> with SingleTickerProviderStateMixin {
  //在这里标签页面使用的是TabView所以需要创建一个控制器
  TabController tabController;
  UserEntity user;

  //页面初始化方法
  @override
  void initState() {
    super.initState();
    //初始化
    tabController = new TabController(length: 3, vsync: this);

   readLocalJson().then((value) {
      setState(() {
        user = UserEntity.fromJson(value);
      });
   });
  }
  
  Future<dynamic> readLocalJson() async {
    var responseStr = await rootBundle.loadString('mock/user.json');
    var responseJson = json.decode(responseStr);
//    print('==============='+responseStr);
    return responseJson;
  }

  //页面销毁回调生命周期
  @override
  void dispose() {
    tabController.dispose();
  }

  //页面构建方法
  @override
  Widget build(BuildContext context) {
    //构建页面的主体
    return Scaffold(
      //下拉刷新
      body: buildNestedScrollView(),

    );
  }

  //NestedScrollView 的基本使用
  Widget buildNestedScrollView() {
    //滑动视图
    return NestedScrollView(
      //配置可折叠的头布局
      headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
        return [buildSliverAppBar()];
      },
      //页面的主体内容
      body: buidChildWidget(),
    );
  }

  //SliverAppBar
  //flexibleSpace可折叠的内容区域
  buildSliverAppBar() {
    return SliverAppBar(
      backgroundColor: Const.themeColor,
      title: buildHeader(),
      actions: <Widget>[
        IconButton(
            icon: Image.asset(Const.tabbarAssets + 'icon_home_add.png'),
            onPressed: () {})
      ],
      //标题居中
      centerTitle: true,
      //当此值为true时 SliverAppBar 会固定在页面顶部
      //当此值为fase时 SliverAppBar 会随着滑动向上滑动
      pinned: true,
      //当值为true时 SliverAppBar设置的title会随着上滑动隐藏
      //然后配置的bottom会显示在原AppBar的位置
      //当值为false时 SliverAppBar设置的title会不会隐藏
      //然后配置的bottom会显示在原AppBar设置的title下面
      floating: false,
      //当snap配置为true时，向下滑动页面，SliverAppBar（以及其中配置的flexibleSpace内容）会立即显示出来，
      //反之当snap配置为false时，向下滑动时，只有当ListView的数据滑动到顶部时，SliverAppBar才会下拉显示出来。
      snap: false,
      elevation: 0.0,
      //展开的高度
      expandedHeight: 483,
      //AppBar下的内容区域
      flexibleSpace: FlexibleSpaceBar(
        //背景
        //配置的是一个widget也就是说在这里可以使用任意的
        //Widget组合 在这里直接使用的是一个图片
        background: buildFlexibleSpaceWidget(),
      ),
      bottom: buildFlexibleTooBarWidget(),
    );
  }

  //通常在用到 PageView + BottomNavigationBar 或者 TabBarView + TabBar 的时候
  //大家会发现当切换到另一页面的时候, 前一个页面就会被销毁, 再返回前一页时, 页面会被重建,
  //随之数据会重新加载, 控件会重新渲染 带来了极不好的用户体验.
  //由于TabBarView内部也是用的是PageView, 因此两者的解决方式相同
  //页面的主体内容
  Widget buidChildWidget() {
    return TabBarView(
      controller: tabController,
      children: <Widget>[
        ItemPage(1),
        ItemPage(2),
        ItemPage(3),
      ],
    );
  }

  //构建SliverAppBar的标题title
  buildHeader() {
    return Text(user?.nickname ?? '个人主页');
  }

  //显示图片与角标区域Widget构建
  buildFlexibleSpaceWidget() {
    return Column(
      children: [
        //图片
        Container(
          color: Const.themeColor,
          height: 240,
          child: CachedNetworkImage(
            imageUrl: "https://pb3.pstatp.com/obj/dbc1001cd29ccc479f7f",
            placeholder: (context, url) => CircularProgressIndicator(),
            errorWidget: (context, url, error) => Icon(Icons.error),
            height: 240,
            width: MediaQuery.of(context).size.width,
            fit: BoxFit.fill,
          ),
        ),
        //资料
        buildProfileWidget(),
      ],
    );
  }

  Widget buildProfileWidget() {
     return Container(
       color: Const.themeColor,
       height: 243,
       child: Column(
         crossAxisAlignment: CrossAxisAlignment.start,
         children: <Widget>[
           Row(
             children: <Widget>[
               Container(
                 decoration: BoxDecoration(
                   border: Border.all(width: 3.0, color: Const.themeColor),
                   borderRadius: BorderRadius.circular(40)
                 ),
                 transform: Matrix4.translationValues(0.0, -20.0, 0.0),
                 margin: EdgeInsets.only(left: 15),
                 child:ClipRRect(
                   borderRadius: BorderRadius.circular(40),
                   child: CachedNetworkImage(
                     imageUrl:
                     "https://pb3.pstatp.com/obj/dbc1001cd29ccc479f7f",
                     placeholder: (context, url) =>
                         CircularProgressIndicator(),
                     errorWidget: (context, url, error) => Icon(Icons.error),
                     height: 80,
                     width: 80,
                     fit: BoxFit.fill,
                   ),
                 ) ,
               ),
               Expanded(
                 child: Row(
                   mainAxisAlignment: MainAxisAlignment.end,
                   children: <Widget>[
                     Container(
                       margin: EdgeInsets.only(right: 5),
                       child: ClipRRect(
                         borderRadius: BorderRadius.circular(4),
                         child: Container(
                           height: 40,
                           padding: EdgeInsets.only(left: 40, right: 40),
                           color: Colors.white54,
                           child: InkWell(
                             child: Center(
                               child: Text('编辑资料',
                                   style: TextStyle(
                                       color: Colors.white54, fontSize: 15)),
                             ),
                             onTap: () {
                               Navigator.of(context).push(new MaterialPageRoute(builder: (context) {
                                 return UserProfilePage(user);
                               }));
                             },
                           ),
                         ),
                       ),
                     ),
                     Container(
                       margin: EdgeInsets.only(right: 15),
                       child: ClipRRect(
                         borderRadius: BorderRadius.circular(4),
                         child: Container(
                           height: 40,
                           padding: EdgeInsets.only(left: 10, right: 10),
                           color: Colors.white54,
                           child: InkWell(
                             child: Center(
                               child: Text('+好友',
                                   style: TextStyle(
                                       color: Colors.white54, fontSize: 15)),
                             ),
                             onTap: () {
                               print('-----------');
                             },
                           ),
                         ),
                       ),
                     ),
                   ],
                 ),
               ),
             ],
           ),
           Container(
             margin: EdgeInsets.only(top: 10, bottom: 10, left: 15),
             child: Text(user?.nickname ?? '抖音昵称',
                 style: TextStyle(color: Colors.white, fontSize: 18)),
           ),
           Container(
             margin: EdgeInsets.only(top: 0, bottom: 5, left: 15),
             child: Text('抖音号:' + (user?.shortId ?? '3452345'),
                 style: TextStyle(color: Colors.white, fontSize: 12)),
           ),
           Container(
             margin: EdgeInsets.only(top: 5, bottom: 5),
             child: Center(
               child: Container(
                 height: 0.5,
                 color: Colors.white,
                 width: MediaQuery.of(context).size.width - 30,
               ),
             ),
           ),
           Container(
             margin: EdgeInsets.only(top: 0, bottom: 10, left: 15, right: 15),
             child: Text(user?.signature ?? '该家伙很懒，什么都没有留下',
                 style: TextStyle(color: Colors.white70, fontSize: 15)),
           ),
           Container(
             margin: EdgeInsets.only(top: 0, bottom: 10, left: 15, right: 15),
             child: InkWell(
               onTap: (){
//                 ToastUtil.showInfo('前往个人资料');
                 Navigator.of(context).push(new MaterialPageRoute(builder: (context) {
                  return UserProfilePage(user);
                 }));
               },
               child: Row(
                 children: <Widget>[
                   profileWidget(user?.gender == 2 ? '♂':'♀'),
                   profileWidget(user?.region ?? '香港'),
                   profileWidget(user?.schoolName ?? '增加学校等标签'),
                 ],
               ),
             ),
           ),
           Container(
             margin: EdgeInsets.only(left: 15, right: 15),
             child: Row(children: <Widget>[
               fansWidget(user?.totalFavorited.toString() ?? '99w', '获赞'),
               fansWidget(user?.followingCount.toString() ?? '999w', '关注'),
               fansWidget(user?.followerCount.toString() ?? '10w', '粉丝')
             ]),
           ),
         ],
       ),
     );
  }

  //[SliverAppBar]的bottom属性配制
  Widget buildFlexibleTooBarWidget() {
    //[PreferredSize]用于配置在AppBar或者是SliverAppBar
    //的bottom中 实现 PreferredSizeWidget
    return PreferredSize(
      //定义大小
      preferredSize: Size(MediaQuery.of(context).size.width, 44),
      //配置任意的子Widget
      child: Container(
        alignment: Alignment.center,
        child: Container(
          color: Const.themeColor,
          //随着向上滑动，TabBar的宽度逐渐增大
          //父布局Container约束为 center对齐
          //所以程现出来的是中间x轴放大的效果
          width: MediaQuery.of(context).size.width,
          child: TabBar(
            indicatorColor: Colors.yellow,
            controller: tabController,
            tabs: <Tab>[
              Tab(text: '作品' + user?.awemeCount.toString() ?? "0"),
              Tab(text: '动态' + user?.storyCount.toString() ?? "0"),
              Tab(text: '喜欢' + user?.favoritingCount.toString() ?? "0"),
            ]
          ),
        ),
      ),
    );
  }
}

Widget profileWidget(text) {
  return Container(
    color: Colors.white38,
    margin: EdgeInsets.only(right: 5),
    padding: EdgeInsets.only(left: 4, right: 4),
    child: Text(text,
          style:
          TextStyle(color: Colors.white54, fontSize: 12)),
    );
}

Widget fansWidget(title, desc){
  return Container(
    margin: EdgeInsets.only(right: 5),
    child: InkWell(
      child: Row(
        children: <Widget>[
          Text(title,
              style:
              TextStyle(color: Colors.white, fontSize: 15)),
          Text(desc,
              style: TextStyle(
                  color: Colors.white54, fontSize: 15)),
        ],
      ),
      onTap: () {

      },

    ),
  );
}

class ItemPage extends StatefulWidget {
  int pageIndex;

  ItemPage(this.pageIndex);
  @override
  _ItemPageState createState() => _ItemPageState();
}

class _ItemPageState extends State<ItemPage> with AutomaticKeepAliveClientMixin {

  List<AwemeEntity> list = [];
  var total = 0;
  @override
  bool get wantKeepAlive => true;

  EasyRefreshController _controller;

  @override
  void initState() {
    super.initState();
    _controller = EasyRefreshController();
    getDataList();
  }

  List<AwemeEntity> getDataList() {
//    print('----------'+widget.pageIndex.toString());
    if(widget.pageIndex == 2) return list; ///直接返回，不处理
    if(total == list.length && total !=0) {
      _controller.finishLoad(success: true, noMore: true);
      return list;
    } else {
      _controller.finishLoad();
    }
    var file = "";
    if(widget.pageIndex == 1) {
      file = 'mock/awemes.json';
    } else if(widget.pageIndex == 3) {
      file = 'mock/favorites.json';
    }
    rootBundle.loadString(file).then((responseStr) {
        var responseJson = json.decode(responseStr);
        total = responseJson['total_count'];
        for(var aweme in responseJson['data']) {
          AwemeEntity a = AwemeEntity.fromJson(aweme);
          list.add(a);
        }
        setState(() {
          list = list;
        });
//        print(list);
        return list;
    });
  }

  List<Widget> getWidgetList() {
    List<AwemeEntity> list1 = getDataList();
    if(list.length == 0){
      _controller.finishLoad(success: true, noMore: true);
    } else {
      _controller.finishLoad();
    }
    list.addAll(list1);
    return list.map((item) => getItemContainer(item)).toList();
  }

  Widget getTopOrNot(AwemeEntity item) {
      if(item.isTop != 0){
        return Center(
          child: Container(
            padding: EdgeInsets.only(left: 3, right: 3),
            color: Colors.yellow,
            child: Text('置顶', style: TextStyle(color: Colors.black45),),
          ),
        );
      }
      return Container();
  }

  Widget getItemContainer(AwemeEntity item) {
    return GestureDetector(
      onTap: (){
        Navigator.push(context, MaterialPageRoute(builder: (context){
           return PlayPage(item);
        }));
      },
      child: Container(
          alignment: Alignment.center,
          child: Stack(
            children: <Widget>[
              CachedNetworkImage(
                imageUrl: item.video.originCover.urlList[0],
                errorWidget: (context, url, error) => Icon(Icons.error),
                fit: BoxFit.fitWidth,
              ),
              Positioned(
                child: getTopOrNot(item),
                top: 5,
                left: 5,
              ),
              Positioned(child:
              Row(
                children: <Widget>[
                  Icon(Icons.favorite, color: Colors.white),
                  Text(item?.statistics?.diggCount.toString() ?? "", style: TextStyle(color: Colors.white),),
                ],
              ),
                bottom: 5,
                left: 5,
              ),
            ],
          )
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Const.themeColor,
      child: list.length > 0 ? EasyRefresh(
          controller: _controller,
          onLoad: () async {
//        await Future.delayed(Duration(seconds: 2), () {
//          if (mounted) {
//            setState(() {
//              print('+++++++++++++++');
//            });
//          }
//        });
            getDataList();
          },
          child: GridView.builder(
            itemCount: list.length,
            itemBuilder: (BuildContext context, int index) {
              //Widget Function(BuildContext context, int index)
              return getItemContainer(list[index]);
            },
            //SliverGridDelegateWithFixedCrossAxisCount 构建一个横轴固定数量Widget
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              //横轴元素个数
              crossAxisCount: 3,
              //纵轴间距
              mainAxisSpacing: 1.0,
              //横轴间距
              crossAxisSpacing: 1.0,
//            padding: EdgeInsets.all(0),
              //子Widget宽高比例
              childAspectRatio: 0.5624,
            ),
          )
      ) : Center(
        child: Text('暂无数据', style: TextStyle(color: Colors.white70),),
      ),
    );
  }
}


