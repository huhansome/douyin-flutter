import 'package:flutter/material.dart';
import 'package:myproject/bean/User_entity.dart';
import 'package:myproject/util/Const.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:myproject/util/ToastUtil.dart';

class UserProfilePage extends StatefulWidget {
  final UserEntity user;
  UserProfilePage(this.user);

  @override
  _UserProfilePageState createState() => _UserProfilePageState();
}

class _UserProfilePageState extends State<UserProfilePage> {

  // 列表项
  Widget _buildListItem(BuildContext context, int index){
    return Container(
      padding: EdgeInsets.only(left: 16, right: 8),
      child: Row(
        children: <Widget>[
          Container(
            width: 100,
            child: Text(dataSource[index].keys.first, style: TextStyle(color: Colors.white),),
          ),
          Expanded(
              child:Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text(dataSource[index].values.first, style: TextStyle(color: Colors.white70)),
                  Icon(Icons.keyboard_arrow_right, color: Colors.white70,)
                ],
              ),
          )
        ],
      ),
    );
  }

  List<Map<String, String>> dataSource = [];

  @override
  void initState() {
    super.initState();
    dataSource = [{"名字": widget.user?.nickname ?? ''},
    {"抖音号": widget.user?.shortId ?? ''},
    {"简介": widget.user?.signature ?? '暂无内容'},
    {"性别": widget.user != null? (widget.user.gender == 2 ? '男': '女') : '未知'},
    {"生日": widget.user?.birthday ?? ''},
    {"地区": widget.user?.region ?? ''},
    {"学校（选填）": widget.user?.schoolName ?? '请编辑学校信息'},
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Const.themeColor,
      appBar: AppBar(
        title: Text('编辑个人资料'),
      ),
      body: Container(
        color: Const.themeColor,
        child: CustomScrollView(
          slivers: <Widget>[
            // 如果不是Sliver家族的Widget，需要使用SliverToBoxAdapter做层包裹
            SliverToBoxAdapter(
              child: Container(
                height: 180,
                color: Const.themeColor,
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      GestureDetector(
                        onTap: (){
                          ToastUtil.showInfo('更换头像');
                        },
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(40),
                          child: CachedNetworkImage(
                            imageUrl:
                            widget.user?.avatarThumb?.urlList[0] ?? '',
                            placeholder: (context, url) =>
                                CircularProgressIndicator(),
                            errorWidget: (context, url, error) => Icon(Icons.error),
                            height: 80,
                            width: 80,
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                       Container(
                         margin: EdgeInsets.only(top: 10),
                          child: Text('点击更换头像', style: TextStyle(color: Colors.white),),
                       ),
                    ],
                  )
                ),
              ),
            ),
            // 当列表项高度固定时，使用 SliverFixedExtendList 比 SliverList 具有更高的性能
            SliverFixedExtentList(
                delegate: SliverChildBuilderDelegate(_buildListItem, childCount: dataSource != null ? dataSource.length: 0),
                itemExtent: 48.0
            )
          ],
        ),
      )
    );
  }
}

