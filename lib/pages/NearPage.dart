import 'package:flutter/material.dart';
import 'package:myproject/util/Const.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:myproject/bean/Aweme_entity.dart';
import 'package:myproject/pages/PlayPage.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'dart:math';

class NearPage extends StatefulWidget {
  @override
  _NearPageState createState() => _NearPageState();
}

class _NearPageState extends State<NearPage> with TickerProviderStateMixin {
  List<AwemeEntity> list = [];
  var total = 0;
  EasyRefreshController _controller;
  List<String> place = ['成都','重庆', '北京', '上海','深圳','广州','南京','杭州'];

   @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller = EasyRefreshController();
     getDataList();
   }

  List<AwemeEntity> getDataList() {
    if(total == list.length && total !=0) {
      _controller.finishLoad(success: true, noMore: true);
      return list;
    } else {
      _controller.finishLoad();
    }
    rootBundle.loadString('mock/awemes.json').then((responseStr) {
      var responseJson = json.decode(responseStr);
      total = responseJson['total_count'];
      for(var aweme in responseJson['data']) {
        AwemeEntity a = AwemeEntity.fromJson(aweme);
        list.add(a);
      }
      setState(() {
        list = list;
      });
      return list;
    });
  }

  List<Widget> getWidgetList() {
    List<AwemeEntity> list1 = getDataList();
    if(list.length == 0){
      _controller.finishLoad(success: true, noMore: true);
    } else {
      _controller.finishLoad();
    }
    list.addAll(list1);
    return list.map((item) => getItemContainer(item)).toList();
  }
   @override
   void dispose() {
     super.dispose();
   }

  Widget getItemContainer(AwemeEntity item) {
    return GestureDetector(
      onTap: (){
        Navigator.push(context, MaterialPageRoute(builder: (context){
          return PlayPage(item);
        }));
      },
      child: Container(
          alignment: Alignment.center,
          child: Stack(
            children: <Widget>[
              CachedNetworkImage(
                imageUrl: item.video.originCover.urlList[0],
                errorWidget: (context, url, error) => Icon(Icons.error),
                fit: BoxFit.cover,
                width: (MediaQuery.of(context).size.width - 1) * 0.5,
              ),

              Positioned(child:
              Row(
                children: <Widget>[
                  Icon(Icons.place, color: Colors.white),
                  Text(place[new Random().nextInt(place.length)], style: TextStyle(color: Colors.white),),
                ],
              ),
                bottom: 5,
                left: 5,
              ),
            ],
          )
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
      backgroundColor: Const.themeColor,
      shadowColor: Const.themeColor,
      title: const Text('同城')
    ),
      body: Container(
        color: Const.themeColor,
        child: list.length > 0 ? EasyRefresh(
            controller: _controller,
            onLoad: () async {
              getDataList();
            },
            child: StaggeredGridView.countBuilder(
              padding: EdgeInsets.all(1.0),
              crossAxisCount: 4,
              itemCount: list.length,
              itemBuilder: (context, index){
                return getItemContainer(list[index]);
              },
              staggeredTileBuilder: (int index) => new StaggeredTile.count(
                2, index == 0 ? 2.5 : 3, ),
              mainAxisSpacing: 1.0,
              crossAxisSpacing: 1.0,
            )
        ) : Center(
          child: Text('暂无数据', style: TextStyle(color: Colors.white70),),
        ),
      )
    );
  }
}

