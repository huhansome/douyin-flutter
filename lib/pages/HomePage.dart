import 'package:flutter/material.dart';
import '../util/Const.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:marquee/marquee.dart';
import 'package:myproject/bean/Aweme_entity.dart';
import 'package:fijkplayer/fijkplayer.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin, TickerProviderStateMixin {
  TabController tabController;
  AnimationController controller;
  final FijkPlayer player = FijkPlayer();
  List<AwemeEntity> list = [];
  var total = 0;
  var currentIndex = 1;
  var file = "mock/awemes.json";
  AwemeEntity currentAweme;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    loadLocalfile(file);

    tabController = new TabController(length: 2, vsync: this, initialIndex: 1);
    controller =
        AnimationController(duration: const Duration(seconds: 5), vsync: this);
    //动画开始、结束、向前移动或向后移动时会调用StatusListener

    controller.forward();
    controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        //动画从 controller.forward() 正向执行 结束时会回调此方法
        print("status is completed");
        //重置起点
        controller.reset();
        //开启
        controller.forward();
      } else if (status == AnimationStatus.dismissed) {
        //动画从 controller.reverse() 反向执行 结束时会回调此方法
        print("status is dismissed");
      } else if (status == AnimationStatus.forward) {
        print("status is forward");
        //执行 controller.forward() 会回调此状态
      } else if (status == AnimationStatus.reverse) {
        //执行 controller.reverse() 会回调此状态
        print("status is reverse");
      }
    });
  }

  loadLocalfile(file) {
    rootBundle.loadString(file).then((responseStr) {
      var responseJson = json.decode(responseStr);
      total = responseJson['total_count'];
      List<AwemeEntity> list1 = [];
      for (var aweme in responseJson['data']) {
        AwemeEntity a = AwemeEntity.fromJson(aweme);
        list1.add(a);
      }
      setState(() {
        list.addAll(list1);
      });
      if (list.length > 0) {
        currentAweme = list[0];
        player.setDataSource(currentAweme.video.playAddr.urlList[0],
            autoPlay: true);
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    player.release();
    controller.dispose();
    super.dispose();
  }

  List<Widget> buildPageItemView() {
    List<Widget> _pages = [];
    for (int i = 0; i < list.length; i++) {
      var aweme = list[i];
      Widget container = Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Stack(
            children: <Widget>[
              Positioned(
                child: Column(
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.5 - 88),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(25),
                          child: CachedNetworkImage(
                            imageUrl: aweme.author.avatarThumb.urlList[0],
                            errorWidget: (context, url, error) =>
                                Icon(Icons.error),
                            height: 50,
                            width: 50,
                            fit: BoxFit.fill,
                          ),
                        )),
                    Container(
                      margin: EdgeInsets.only(top: 30),
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.favorite_sharp,
                            color: Colors.white,
                            size: 30,
                          ),
                          Text(
                            aweme?.statistics?.diggCount.toString() ?? "999",
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 30),
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.message,
                            color: Colors.white,
                            size: 30,
                          ),
                          Text(
                            aweme?.statistics?.commentCount.toString() ?? "999",
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 30),
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.share,
                            color: Colors.white,
                            size: 30,
                          ),
                          Text(
                            aweme?.statistics?.shareCount.toString() ?? "8998",
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(top: 30),
                        child: RotationTransition(
                          turns: controller,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(30),
                            child: CachedNetworkImage(
                              imageUrl: aweme.music.coverThumb.urlList[0],
                              errorWidget: (context, url, error) =>
                                  Icon(Icons.error),
                              height: 60,
                              width: 60,
                              fit: BoxFit.fill,
                            ),
                          ),
                        )),
                  ],
                ),
                right: 10,
                width: 80,
              ),
              Positioned(
                bottom: 80,
                right: 80,
                left: 15,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text(
                          "@" + aweme?.author?.nickname ?? "",
                          style: TextStyle(color: Colors.white),
                        ),
                        Text(
                          "・" + "几天前",
                          style: TextStyle(color: Colors.white70, fontSize: 12),
                        ),
                      ],
                    ),
                    Text(aweme.desc, style: TextStyle(color: Colors.white)),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.music_note,
                          color: Colors.white,
                          size: 20,
                        ),
                        ConstrainedBox(
                            constraints: BoxConstraints(
                                minWidth: 120,
                                minHeight: 20,
                                maxWidth: MediaQuery.of(context).size.width -
                                    80 -
                                    15 -
                                    40,
                                maxHeight: 20),
                            child: Marquee(
                              text:
                                  aweme?.music?.author ?? "" + "  " + aweme?.music?.title ?? "",
                              blankSpace: 40,
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ))
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ));
      _pages.add(container);
    }
    return _pages;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        leading: IconButton(
            icon: Icon(Icons.live_tv),
            onPressed: () {
              Fluttertoast.showToast(
                  msg: '前往直播页面', gravity: ToastGravity.CENTER);
            }),
        title: ConstrainedBox(
          constraints: BoxConstraints(
              minWidth: 120, minHeight: 44, maxWidth: 140, maxHeight: 44),
          child: TabBar(
            indicatorSize: TabBarIndicatorSize.label,
            indicatorColor: Colors.white,
            unselectedLabelColor: Colors.white54,
            controller: tabController,
            tabs: <Widget>[Tab(text: '关注'), Tab(text: '推荐')],
            onTap: (index) {
              if (index == currentIndex) return;
              currentIndex = index;
              if (currentIndex == 0) {
                file = 'mock/favorites.json';
              } else {
                file = 'mock/awemes.json';
              }
              list = [];
              player.reset().then((value) {
                loadLocalfile(file);
              });
            },
          ),
        ),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.search),
              onPressed: () {
                Fluttertoast.showToast(
                    msg: '前往搜索', gravity: ToastGravity.CENTER);
              })
        ],
      ),
      body: list.length > 0
          ? Stack(
              children: <Widget>[
                FijkView(
                  fit: FijkFit.fill,
                  player: player,
                  fs: true,
                  cover:
                      Image.network(currentAweme.video.originCover.urlList[0])
                          .image,
                ),
                PageView(
                  onPageChanged: (page) {
                    setState(() {
                      currentAweme = list[page];
                    });

                    player.reset().then((value) {
                      player.setDataSource(
                          currentAweme.video.playAddr.urlList[0],
                          autoPlay: true);
                    });
                  },
                  children: buildPageItemView(),
                  //设置滑动方向
                  scrollDirection: Axis.vertical,
                )
              ],
            )
          : Center(
              child: Text(
                '暂无数据',
                style: TextStyle(color: Colors.white70),
              ),
            ),
    );
  }
}
