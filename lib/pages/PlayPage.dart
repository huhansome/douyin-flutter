import 'package:flutter/material.dart';
import 'package:myproject/bean/Aweme_entity.dart';
import 'package:fijkplayer/fijkplayer.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:marquee/marquee.dart';

class PlayPage extends StatefulWidget {
  final AwemeEntity aweme;

  PlayPage(this.aweme);

  @override
  _PlayPageState createState() => _PlayPageState();
}

class _PlayPageState extends State<PlayPage> with TickerProviderStateMixin {
  final FijkPlayer player = FijkPlayer();
  AnimationController controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    player.setDataSource(widget.aweme.video.playAddr.urlList[0],
        autoPlay: true);
    controller =
        AnimationController(duration: const Duration(seconds: 5), vsync: this);
    //动画开始、结束、向前移动或向后移动时会调用StatusListener

    controller.forward();
    controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        //动画从 controller.forward() 正向执行 结束时会回调此方法
        print("status is completed");
        //重置起点
        controller.reset();
        //开启
        controller.forward();
      } else if (status == AnimationStatus.dismissed) {
        //动画从 controller.reverse() 反向执行 结束时会回调此方法
        print("status is dismissed");
      } else if (status == AnimationStatus.forward) {
        print("status is forward");
        //执行 controller.forward() 会回调此状态
      } else if (status == AnimationStatus.reverse) {
        //执行 controller.reverse() 会回调此状态
        print("status is reverse");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            child: Stack(
      children: <Widget>[
        FijkView(
          fs: false,
          fit: FijkFit.fill,
          player: player,
        ),
        Positioned(
          child: Column(
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.5),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(25),
                    child: CachedNetworkImage(
                      imageUrl: widget.aweme.author.avatarThumb.urlList[0],
                      errorWidget: (context, url, error) => Icon(Icons.error),
                      height: 50,
                      width: 50,
                      fit: BoxFit.fill,
                    ),
                  )),
              Container(
                margin: EdgeInsets.only(top: 30),
                child: Column(
                  children: <Widget>[
                    Icon(
                      Icons.favorite_sharp,
                      color: Colors.white,
                      size: 30,
                    ),
                    Text(
                      widget.aweme.statistics.diggCount.toString(),
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 30),
                child: Column(
                  children: <Widget>[
                    Icon(
                      Icons.message,
                      color: Colors.white,
                      size: 30,
                    ),
                    Text(
                      widget.aweme.statistics.commentCount.toString(),
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 30),
                child: Column(
                  children: <Widget>[
                    Icon(
                      Icons.share,
                      color: Colors.white,
                      size: 30,
                    ),
                    Text(
                      widget.aweme.statistics.shareCount.toString(),
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ),
              Container(
                  margin: EdgeInsets.only(top: 30),
                  child: RotationTransition(
                    turns: controller,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(30),
                      child: CachedNetworkImage(
                        imageUrl: widget.aweme.music.coverThumb.urlList[0],
                        errorWidget: (context, url, error) => Icon(Icons.error),
                        height: 60,
                        width: 60,
                        fit: BoxFit.fill,
                      ),
                    ),
                  )),
            ],
          ),
          right: 10,
          width: 80,
        ),
        Positioned(
          bottom: 80,
          right: 80,
          left: 15,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text(
                    "@" + widget.aweme.author.nickname,
                    style: TextStyle(color: Colors.white),
                  ),
                  Text(
                    "・" + "几天前",
                    style: TextStyle(color: Colors.white70, fontSize: 12),
                  ),
                ],
              ),
              Text(widget.aweme.desc, style: TextStyle(color: Colors.white)),
              Row(
                children: <Widget>[
                  Icon(
                    Icons.music_note,
                    color: Colors.white,
                    size: 20,
                  ),
                  ConstrainedBox(
                      constraints: BoxConstraints(
                          minWidth: 120,
                          minHeight: 20,
                          maxWidth:
                              MediaQuery.of(context).size.width - 80 - 15 - 40,
                          maxHeight: 20),
                      child: Marquee(
                        text: widget.aweme.music.author +
                            "  " +
                            widget.aweme.music.title,
                        blankSpace: 40,
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ))
                ],
              ),
            ],
          ),
        ),
      ],
    )));
  }

  @override
  void dispose() {
    player.release();
    controller.dispose();
    super.dispose();
  }
}
