class AwemeEntity {
	LabelTop labelTop;
	List<String> videoLabels;
	bool cmtSwt;
	int vrType;
	Video video;
	bool isVr;
	int bodydanceScore;
	Descendants descendants;
	Music music;
	int isHashTag;
	int rate;
	int scenario;
	bool isRelieve;
	List<String> videoText;
	int createTime;
	Author author;
	ShareInfo shareInfo;
	bool isPgcshow;
	RiskInfos riskInfos;
	String sortLabel;
	int userDigged;
	int isTop;
	List<ChaList> chaList;
	String awemeId;
	List<String> textExtra;
	int authorUserId;
	bool isAds;
	bool canPlay;
	String shareUrl;
	bool lawCriticalCountry;
	String region;
//	List<dynamic> geofencing;
	int awemeType;
	Statistics statistics;
	Status status;
	String desc;

	AwemeEntity({this.labelTop, this.videoLabels, this.cmtSwt, this.vrType, this.video, this.isVr, this.bodydanceScore, this.descendants, this.music, this.isHashTag, this.rate, this.scenario, this.isRelieve, this.videoText, this.createTime, this.author, this.shareInfo, this.isPgcshow, this.riskInfos, this.sortLabel, this.userDigged, this.isTop, this.chaList, this.awemeId, this.textExtra, this.authorUserId, this.isAds, this.canPlay, this.shareUrl, this.lawCriticalCountry, this.region, this.awemeType, this.statistics, this.status, this.desc});

	AwemeEntity.fromJson(Map<String, dynamic> json) {
		labelTop = (json['label_top'] != null) ? new LabelTop.fromJson(json['label_top']) : null;
		if (json['video_labels'] != null) {
			videoLabels = new List<String>();
			json['video_labels'].forEach((v) { videoLabels.add(v); });
		}
		cmtSwt = json['cmt_swt'];
		vrType = json['vr_type'];
		video = json['video'] != null ? new Video.fromJson(json['video']) : null;
		isVr = json['is_vr'];
		bodydanceScore = json['bodydance_score'];
		descendants = json['descendants'] != null ? new Descendants.fromJson(json['descendants']) : null;
		music = json['music'] != null ? new Music.fromJson(json['music']) : null;
		isHashTag = json['is_hash_tag'];
		rate = json['rate'];
		scenario = json['scenario'];
		isRelieve = json['is_relieve'];
		if (json['video_text'] != null) {
			videoText = new List<Null>();
			json['video_text'].forEach((v) { videoText.add(v); });
		}
		createTime = json['create_time'];
		author = json['author'] != null ? new Author.fromJson(json['author']) : null;
		shareInfo = json['share_info'] != null ? new ShareInfo.fromJson(json['share_info']) : null;
		isPgcshow = json['is_pgcshow'];
		riskInfos = json['risk_infos'] != null ? new RiskInfos.fromJson(json['risk_infos']) : null;
		sortLabel = json['sort_label'];
		userDigged = json['user_digged'];
		isTop = json['is_top'];
		if (json['cha_list'] != null) {
			chaList = new List<ChaList>();
			json['cha_list'].forEach((v) { chaList.add(new ChaList.fromJson(v)); });
		}
		awemeId = json['aweme_id'];
		if (json['text_extra'] != null) {
			textExtra = new List<String>();
			json['text_extra'].forEach((v) { textExtra.add(v.toString()); });
		}
		authorUserId = json['author_user_id'];
		isAds = json['is_ads'];
		canPlay = json['can_play'];
		shareUrl = json['share_url'];
		lawCriticalCountry = json['law_critical_country'];
		region = json['region'];
//		if (json['geofencing'] != null) {
//			geofencing = new List<dynamic>();
//			json['geofencing'].forEach((v) { geofencing.add(v); });
//		}
		awemeType = json['aweme_type'];
		statistics = json['statistics'] != null ? new Statistics.fromJson(json['statistics']) : null;
		status = json['status'] != null ? new Status.fromJson(json['status']) : null;
		desc = json['desc'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		if (this.labelTop != null) {
      data['label_top'] = this.labelTop.toJson();
    }
		if (this.videoLabels != null) {
      data['video_labels'] = this.videoLabels.map((v) => v).toList();
    }
		data['cmt_swt'] = this.cmtSwt;
		data['vr_type'] = this.vrType;
		if (this.video != null) {
      data['video'] = this.video.toJson();
    }
		data['is_vr'] = this.isVr;
		data['bodydance_score'] = this.bodydanceScore;
		if (this.descendants != null) {
      data['descendants'] = this.descendants.toJson();
    }
		if (this.music != null) {
      data['music'] = this.music.toJson();
    }
		data['is_hash_tag'] = this.isHashTag;
		data['rate'] = this.rate;
		data['scenario'] = this.scenario;
		data['is_relieve'] = this.isRelieve;
		if (this.videoText != null) {
      data['video_text'] = this.videoText.map((v) => v).toList();
    }
		data['create_time'] = this.createTime;
		if (this.author != null) {
      data['author'] = this.author.toJson();
    }
		if (this.shareInfo != null) {
      data['share_info'] = this.shareInfo.toJson();
    }
		data['is_pgcshow'] = this.isPgcshow;
		if (this.riskInfos != null) {
      data['risk_infos'] = this.riskInfos.toJson();
    }
		data['sort_label'] = this.sortLabel;
		data['user_digged'] = this.userDigged;
		data['is_top'] = this.isTop;
		if (this.chaList != null) {
      data['cha_list'] = this.chaList.map((v) => v.toJson()).toList();
    }
		data['aweme_id'] = this.awemeId;
		if (this.textExtra != null) {
      data['text_extra'] = this.textExtra.map((v) => v).toList();
    }
		data['author_user_id'] = this.authorUserId;
		data['is_ads'] = this.isAds;
		data['can_play'] = this.canPlay;
		data['share_url'] = this.shareUrl;
		data['law_critical_country'] = this.lawCriticalCountry;
		data['region'] = this.region;
//		if (this.geofencing != null) {
//      data['geofencing'] = this.geofencing.map((v) => v).toList();
//    }
		data['aweme_type'] = this.awemeType;
		if (this.statistics != null) {
      data['statistics'] = this.statistics.toJson();
    }
		if (this.status != null) {
      data['status'] = this.status.toJson();
    }
		data['desc'] = this.desc;
		return data;
	}
}

class LabelTop {
	String uri;
	List<String> urlList;

	LabelTop({this.uri, this.urlList});

	LabelTop.fromJson(Map<String, dynamic> json) {
		uri = json['uri'];
		urlList = json['url_list'].cast<String>();
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['uri'] = this.uri;
		data['url_list'] = this.urlList;
		return data;
	}
}

class Video {
	String id;
	String ratio;
	OriginCover originCover;
	PlayAddr playAddr;
	Cover cover;
	int height;
	int width;
	DownloadAddr downloadAddr;
	bool hasWatermark;
	int duration;
	PlayAddrLowbr playAddrLowbr;
	DynamicCover dynamicCover;
	List<BitRate> bitRate;

	Video({this.id, this.ratio, this.originCover, this.playAddr, this.cover, this.height, this.width, this.downloadAddr, this.hasWatermark, this.duration, this.playAddrLowbr, this.dynamicCover, this.bitRate});

	Video.fromJson(Map<String, dynamic> json) {
		id = json['id'];
		ratio = json['ratio'];
		originCover = json['origin_cover'] != null ? new OriginCover.fromJson(json['origin_cover']) : null;
		playAddr = json['play_addr'] != null ? new PlayAddr.fromJson(json['play_addr']) : null;
		cover = json['cover'] != null ? new Cover.fromJson(json['cover']) : null;
		height = json['height'];
		width = json['width'];
		downloadAddr = json['download_addr'] != null ? new DownloadAddr.fromJson(json['download_addr']) : null;
		hasWatermark = json['has_watermark'];
		duration = json['duration'];
		playAddrLowbr = json['play_addr_lowbr'] != null ? new PlayAddrLowbr.fromJson(json['play_addr_lowbr']) : null;
		dynamicCover = json['dynamic_cover'] != null ? new DynamicCover.fromJson(json['dynamic_cover']) : null;
		if (json['bit_rate'] != null) {
			bitRate = new List<BitRate>();
			json['bit_rate'].forEach((v) { bitRate.add(new BitRate.fromJson(v)); });
		}
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['id'] = this.id;
		data['ratio'] = this.ratio;
		if (this.originCover != null) {
      data['origin_cover'] = this.originCover.toJson();
    }
		if (this.playAddr != null) {
      data['play_addr'] = this.playAddr.toJson();
    }
		if (this.cover != null) {
      data['cover'] = this.cover.toJson();
    }
		data['height'] = this.height;
		data['width'] = this.width;
		if (this.downloadAddr != null) {
      data['download_addr'] = this.downloadAddr.toJson();
    }
		data['has_watermark'] = this.hasWatermark;
		data['duration'] = this.duration;
		if (this.playAddrLowbr != null) {
      data['play_addr_lowbr'] = this.playAddrLowbr.toJson();
    }
		if (this.dynamicCover != null) {
      data['dynamic_cover'] = this.dynamicCover.toJson();
    }
		if (this.bitRate != null) {
      data['bit_rate'] = this.bitRate.map((v) => v.toJson()).toList();
    }
		return data;
	}
}

class OriginCover {
	String uri;
	List<String> urlList;

	OriginCover({this.uri, this.urlList});

	OriginCover.fromJson(Map<String, dynamic> json) {
		uri = json['uri'];
		urlList = json['url_list'].cast<String>();
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['uri'] = this.uri;
//		data['url_list'] = this.urlList;
		return data;
	}
}

class PlayAddr {
	String uri;
	List<String> urlList;

	PlayAddr({this.uri, this.urlList});

	PlayAddr.fromJson(Map<String, dynamic> json) {
		uri = json['uri'];
		urlList = json['url_list'].cast<String>();
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['uri'] = this.uri;
		data['url_list'] = this.urlList;
		return data;
	}
}

class Cover {
	String uri;
	List<String> urlList;

	Cover({this.uri, this.urlList});

	Cover.fromJson(Map<String, dynamic> json) {
		uri = json['uri'];
		urlList = json['url_list'].cast<String>();
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['uri'] = this.uri;
		data['url_list'] = this.urlList;
		return data;
	}
}

class DownloadAddr {
	String uri;
	List<String> urlList;

	DownloadAddr({this.uri, this.urlList});

	DownloadAddr.fromJson(Map<String, dynamic> json) {
		uri = json['uri'];
		urlList = json['url_list'].cast<String>();
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['uri'] = this.uri;
		data['url_list'] = this.urlList;
		return data;
	}
}

class PlayAddrLowbr {
	String uri;
	List<String> urlList;

	PlayAddrLowbr({this.uri, this.urlList});

	PlayAddrLowbr.fromJson(Map<String, dynamic> json) {
		uri = json['uri'];
		urlList = json['url_list'].cast<String>();
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['uri'] = this.uri;
		data['url_list'] = this.urlList;
		return data;
	}
}

class DynamicCover {
	String uri;
	List<String> urlList;

	DynamicCover({this.uri, this.urlList});

	DynamicCover.fromJson(Map<String, dynamic> json) {
		uri = json['uri'];
		urlList = json['url_list'].cast<String>();
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['uri'] = this.uri;
		data['url_list'] = this.urlList;
		return data;
	}
}

class BitRate {
	int bitRate;
	String gearName;
	int qualityType;

	BitRate({this.bitRate, this.gearName, this.qualityType});

	BitRate.fromJson(Map<String, dynamic> json) {
		bitRate = json['bit_rate'];
		gearName = json['gear_name'];
		qualityType = json['quality_type'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['bit_rate'] = this.bitRate;
		data['gear_name'] = this.gearName;
		data['quality_type'] = this.qualityType;
		return data;
	}
}

class Descendants {
	String notifyMsg;
	List<String> platforms;

	Descendants({this.notifyMsg, this.platforms});

	Descendants.fromJson(Map<String, dynamic> json) {
		notifyMsg = json['notify_msg'];
		platforms = json['platforms'].cast<String>();
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['notify_msg'] = this.notifyMsg;
		data['platforms'] = this.platforms;
		return data;
	}
}

class Music {
	String id;
	Null uid;
	int status;
	String extra;
	bool isOriginal;
	String offlineDesc;
	int sourcePlatform;
	CoverLarge coverLarge;
	int duration;
	CoverThumb coverThumb;
	CoverHd coverHd;
	int userCount;
	String title;
	PlayUrl playUrl;
	String author;
	String mid;
	CoverMedium coverMedium;
	String idStr;
	bool isRestricted;
	String schemaUrl;

	Music({this.id, this.uid, this.status, this.extra, this.isOriginal, this.offlineDesc, this.sourcePlatform, this.coverLarge, this.duration, this.coverThumb, this.coverHd, this.userCount, this.title, this.playUrl, this.author, this.mid, this.coverMedium, this.idStr, this.isRestricted, this.schemaUrl});

	Music.fromJson(Map<String, dynamic> json) {
		id = json['id'];
		uid = json['uid'];
		status = json['status'];
		extra = json['extra'];
		isOriginal = json['is_original'];
		offlineDesc = json['offline_desc'];
		sourcePlatform = json['source_platform'];
		coverLarge = json['cover_large'] != null ? new CoverLarge.fromJson(json['cover_large']) : null;
		duration = json['duration'];
		coverThumb = json['cover_thumb'] != null ? new CoverThumb.fromJson(json['cover_thumb']) : null;
		coverHd = json['cover_hd'] != null ? new CoverHd.fromJson(json['cover_hd']) : null;
		userCount = json['user_count'];
		title = json['title'];
		playUrl = json['play_url'] != null ? new PlayUrl.fromJson(json['play_url']) : null;
		author = json['author'];
		mid = json['mid'];
		coverMedium = json['cover_medium'] != null ? new CoverMedium.fromJson(json['cover_medium']) : null;
		idStr = json['id_str'];
		isRestricted = json['is_restricted'];
		schemaUrl = json['schema_url'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['id'] = this.id;
		data['uid'] = this.uid;
		data['status'] = this.status;
		data['extra'] = this.extra;
		data['is_original'] = this.isOriginal;
		data['offline_desc'] = this.offlineDesc;
		data['source_platform'] = this.sourcePlatform;
		if (this.coverLarge != null) {
      data['cover_large'] = this.coverLarge.toJson();
    }
		data['duration'] = this.duration;
		if (this.coverThumb != null) {
      data['cover_thumb'] = this.coverThumb.toJson();
    }
		if (this.coverHd != null) {
      data['cover_hd'] = this.coverHd.toJson();
    }
		data['user_count'] = this.userCount;
		data['title'] = this.title;
		if (this.playUrl != null) {
      data['play_url'] = this.playUrl.toJson();
    }
		data['author'] = this.author;
		data['mid'] = this.mid;
		if (this.coverMedium != null) {
      data['cover_medium'] = this.coverMedium.toJson();
    }
		data['id_str'] = this.idStr;
		data['is_restricted'] = this.isRestricted;
		data['schema_url'] = this.schemaUrl;
		return data;
	}
}

class CoverLarge {
	String uri;
	List<String> urlList;

	CoverLarge({this.uri, this.urlList});

	CoverLarge.fromJson(Map<String, dynamic> json) {
		uri = json['uri'];
		urlList = json['url_list'].cast<String>();
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['uri'] = this.uri;
		data['url_list'] = this.urlList;
		return data;
	}
}

class CoverThumb {
	String uri;
	List<String> urlList;

	CoverThumb({this.uri, this.urlList});

	CoverThumb.fromJson(Map<String, dynamic> json) {
		uri = json['uri'];
		urlList = json['url_list'].cast<String>();
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['uri'] = this.uri;
		data['url_list'] = this.urlList;
		return data;
	}
}

class CoverHd {
	String uri;
	List<String> urlList;

	CoverHd({this.uri, this.urlList});

	CoverHd.fromJson(Map<String, dynamic> json) {
		uri = json['uri'];
		urlList = json['url_list'].cast<String>();
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['uri'] = this.uri;
		data['url_list'] = this.urlList;
		return data;
	}
}

class PlayUrl {
	String uri;
	List<String> urlList;

	PlayUrl({this.uri, this.urlList});

	PlayUrl.fromJson(Map<String, dynamic> json) {
		uri = json['uri'];
		urlList = json['url_list'].cast<String>();
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['uri'] = this.uri;
		data['url_list'] = this.urlList;
		return data;
	}
}

class CoverMedium {
	String uri;
	List<String> urlList;

	CoverMedium({this.uri, this.urlList});

	CoverMedium.fromJson(Map<String, dynamic> json) {
		uri = json['uri'];
		urlList = json['url_list'].cast<String>();
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['uri'] = this.uri;
		data['url_list'] = this.urlList;
		return data;
	}
}

class Author {
	String uid;
	int commentSetting;
	String youtubeChannelTitle;
	int followingCount;
	String shareQrcodeUri;
	String youtubeChannelId;
	AvatarLarger avatarLarger;
	String enterpriseVerifyReason;
	String originalMusicQrcode;
	bool storyOpen;
	int userRate;
	int liveVerify;
	String shortId;
	bool isGovMediaVip;
	int secret;
	String accountRegion;
	int reflowPageGid;
	AvatarThumb avatarThumb;
	bool isBindedWeibo;
	bool isVerified;
	String videoIconVirtualUri;
	bool hideSearch;
	bool withCommerceEntry;
	String schoolName;
	String customVerify;
	int awemeCount;
	int specialLock;
	bool userCanceled;
	int shieldCommentNotice;
	int totalFavorited;
	int favoritingCount;
	bool hideLocation;
	int gender;
	VideoIcon videoIcon;
	String schoolPoiId;
	bool hasEmail;
	int liveAgreement;
	Activity activity;
	String region;
	bool preventDownload;
	String weiboSchema;
	String bindPhone;
	String weiboUrl;
	int liveAgreementTime;
	String weiboName;
	String twitterId;
	int commerceUserLevel;
	int createTime;
	int storyCount;
	String verifyInfo;
	String googleAccount;
	int constellation;
	int appleAccount;
	bool acceptPrivatePolicy;
	int needRecommend;
	int shieldDiggNotice;
	int verificationType;
	int followerStatus;
	int neiguangShield;
	int roomId;
	AvatarMedium avatarMedium;
	int followerCount;
	int authorityStatus;
	bool hasOrders;
	int reflowPageUid;
	String birthday;
	bool isAdFake;
	int duetSetting;
	String nickname;
	int shieldFollowNotice;
	Null originalMusicCover;
	int followStatus;
	String insId;
	int uniqueIdModifyTime;
	bool isPhoneBinded;
	int schoolType;
	int status;
	String twitterName;
	String avatarUri;
	String signature;
	String weiboVerify;
	String uniqueId;
//	List<dynamic> geofencing;

	Author({this.uid, this.commentSetting, this.youtubeChannelTitle, this.followingCount, this.shareQrcodeUri, this.youtubeChannelId, this.avatarLarger, this.enterpriseVerifyReason, this.originalMusicQrcode, this.storyOpen, this.userRate, this.liveVerify, this.shortId, this.isGovMediaVip, this.secret, this.accountRegion, this.reflowPageGid, this.avatarThumb, this.isBindedWeibo, this.isVerified, this.videoIconVirtualUri, this.hideSearch, this.withCommerceEntry, this.schoolName, this.customVerify, this.awemeCount, this.specialLock, this.userCanceled, this.shieldCommentNotice, this.totalFavorited, this.favoritingCount, this.hideLocation, this.gender, this.videoIcon, this.schoolPoiId, this.hasEmail, this.liveAgreement, this.activity, this.region, this.preventDownload, this.weiboSchema, this.bindPhone, this.weiboUrl, this.liveAgreementTime, this.weiboName, this.twitterId, this.commerceUserLevel, this.createTime, this.storyCount, this.verifyInfo, this.googleAccount, this.constellation, this.appleAccount, this.acceptPrivatePolicy, this.needRecommend, this.shieldDiggNotice, this.verificationType, this.followerStatus, this.neiguangShield, this.roomId, this.avatarMedium, this.followerCount, this.authorityStatus, this.hasOrders, this.reflowPageUid, this.birthday, this.isAdFake, this.duetSetting, this.nickname, this.shieldFollowNotice, this.originalMusicCover, this.followStatus, this.insId, this.uniqueIdModifyTime, this.isPhoneBinded, this.schoolType, this.status, this.twitterName, this.avatarUri, this.signature, this.weiboVerify, this.uniqueId});

	Author.fromJson(Map<String, dynamic> json) {
		uid = json['uid'];
		commentSetting = json['comment_setting'];
		youtubeChannelTitle = json['youtube_channel_title'];
		followingCount = json['following_count'];
		shareQrcodeUri = json['share_qrcode_uri'];
		youtubeChannelId = json['youtube_channel_id'];
		avatarLarger = json['avatar_larger'] != null ? new AvatarLarger.fromJson(json['avatar_larger']) : null;
		enterpriseVerifyReason = json['enterprise_verify_reason'];
		originalMusicQrcode = json['original_music_qrcode'];
		storyOpen = json['story_open'];
		userRate = json['user_rate'];
		liveVerify = json['live_verify'];
		shortId = json['short_id'];
		isGovMediaVip = json['is_gov_media_vip'];
		secret = json['secret'];
		accountRegion = json['account_region'];
		reflowPageGid = json['reflow_page_gid'];
		avatarThumb = json['avatar_thumb'] != null ? new AvatarThumb.fromJson(json['avatar_thumb']) : null;
		isBindedWeibo = json['is_binded_weibo'];
		isVerified = json['is_verified'];
		videoIconVirtualUri = json['video_icon_virtual_URI'];
		hideSearch = json['hide_search'];
		withCommerceEntry = json['with_commerce_entry'];
		schoolName = json['school_name'];
		customVerify = json['custom_verify'];
		awemeCount = json['aweme_count'];
		specialLock = json['special_lock'];
		userCanceled = json['user_canceled'];
		shieldCommentNotice = json['shield_comment_notice'];
		totalFavorited = json['total_favorited'];
		favoritingCount = json['favoriting_count'];
		hideLocation = json['hide_location'];
		gender = json['gender'];
		videoIcon = json['video_icon'] != null ? new VideoIcon.fromJson(json['video_icon']) : null;
		schoolPoiId = json['school_poi_id'];
		hasEmail = json['has_email'];
		liveAgreement = json['live_agreement'];
		activity = json['activity'] != null ? new Activity.fromJson(json['activity']) : null;
		region = json['region'];
		preventDownload = json['prevent_download'];
		weiboSchema = json['weibo_schema'];
		bindPhone = json['bind_phone'];
		weiboUrl = json['weibo_url'];
		liveAgreementTime = json['live_agreement_time'];
		weiboName = json['weibo_name'];
		twitterId = json['twitter_id'];
		commerceUserLevel = json['commerce_user_level'];
		createTime = json['create_time'];
		storyCount = json['story_count'];
		verifyInfo = json['verify_info'];
		googleAccount = json['google_account'];
		constellation = json['constellation'];
		appleAccount = json['apple_account'];
		acceptPrivatePolicy = json['accept_private_policy'];
		needRecommend = json['need_recommend'];
		shieldDiggNotice = json['shield_digg_notice'];
		verificationType = json['verification_type'];
		followerStatus = json['follower_status'];
		neiguangShield = json['neiguang_shield'];
		roomId = json['room_id'];
		avatarMedium = json['avatar_medium'] != null ? new AvatarMedium.fromJson(json['avatar_medium']) : null;
		followerCount = json['follower_count'];
		authorityStatus = json['authority_status'];
		hasOrders = json['has_orders'];
		reflowPageUid = json['reflow_page_uid'];
		birthday = json['birthday'];
		isAdFake = json['is_ad_fake'];
		duetSetting = json['duet_setting'];
		nickname = json['nickname'];
		shieldFollowNotice = json['shield_follow_notice'];
		originalMusicCover = json['original_music_cover'];
		followStatus = json['follow_status'];
		insId = json['ins_id'];
		uniqueIdModifyTime = json['unique_id_modify_time'];
		isPhoneBinded = json['is_phone_binded'];
		schoolType = json['school_type'];
		status = json['status'];
		twitterName = json['twitter_name'];
		avatarUri = json['avatar_uri'];
		signature = json['signature'];
		weiboVerify = json['weibo_verify'];
		uniqueId = json['unique_id'];
//		geofencing = json['geofencing'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['uid'] = this.uid;
		data['comment_setting'] = this.commentSetting;
		data['youtube_channel_title'] = this.youtubeChannelTitle;
		data['following_count'] = this.followingCount;
		data['share_qrcode_uri'] = this.shareQrcodeUri;
		data['youtube_channel_id'] = this.youtubeChannelId;
		if (this.avatarLarger != null) {
      data['avatar_larger'] = this.avatarLarger.toJson();
    }
		data['enterprise_verify_reason'] = this.enterpriseVerifyReason;
		data['original_music_qrcode'] = this.originalMusicQrcode;
		data['story_open'] = this.storyOpen;
		data['user_rate'] = this.userRate;
		data['live_verify'] = this.liveVerify;
		data['short_id'] = this.shortId;
		data['is_gov_media_vip'] = this.isGovMediaVip;
		data['secret'] = this.secret;
		data['account_region'] = this.accountRegion;
		data['reflow_page_gid'] = this.reflowPageGid;
		if (this.avatarThumb != null) {
      data['avatar_thumb'] = this.avatarThumb.toJson();
    }
		data['is_binded_weibo'] = this.isBindedWeibo;
		data['is_verified'] = this.isVerified;
		data['video_icon_virtual_URI'] = this.videoIconVirtualUri;
		data['hide_search'] = this.hideSearch;
		data['with_commerce_entry'] = this.withCommerceEntry;
		data['school_name'] = this.schoolName;
		data['custom_verify'] = this.customVerify;
		data['aweme_count'] = this.awemeCount;
		data['special_lock'] = this.specialLock;
		data['user_canceled'] = this.userCanceled;
		data['shield_comment_notice'] = this.shieldCommentNotice;
		data['total_favorited'] = this.totalFavorited;
		data['favoriting_count'] = this.favoritingCount;
		data['hide_location'] = this.hideLocation;
		data['gender'] = this.gender;
		if (this.videoIcon != null) {
      data['video_icon'] = this.videoIcon.toJson();
    }
		data['school_poi_id'] = this.schoolPoiId;
		data['has_email'] = this.hasEmail;
		data['live_agreement'] = this.liveAgreement;
		if (this.activity != null) {
      data['activity'] = this.activity.toJson();
    }
		data['region'] = this.region;
		data['prevent_download'] = this.preventDownload;
		data['weibo_schema'] = this.weiboSchema;
		data['bind_phone'] = this.bindPhone;
		data['weibo_url'] = this.weiboUrl;
		data['live_agreement_time'] = this.liveAgreementTime;
		data['weibo_name'] = this.weiboName;
		data['twitter_id'] = this.twitterId;
		data['commerce_user_level'] = this.commerceUserLevel;
		data['create_time'] = this.createTime;
		data['story_count'] = this.storyCount;
		data['verify_info'] = this.verifyInfo;
		data['google_account'] = this.googleAccount;
		data['constellation'] = this.constellation;
		data['apple_account'] = this.appleAccount;
		data['accept_private_policy'] = this.acceptPrivatePolicy;
		data['need_recommend'] = this.needRecommend;
		data['shield_digg_notice'] = this.shieldDiggNotice;
		data['verification_type'] = this.verificationType;
		data['follower_status'] = this.followerStatus;
		data['neiguang_shield'] = this.neiguangShield;
		data['room_id'] = this.roomId;
		if (this.avatarMedium != null) {
      data['avatar_medium'] = this.avatarMedium.toJson();
    }
		data['follower_count'] = this.followerCount;
		data['authority_status'] = this.authorityStatus;
		data['has_orders'] = this.hasOrders;
		data['reflow_page_uid'] = this.reflowPageUid;
		data['birthday'] = this.birthday;
		data['is_ad_fake'] = this.isAdFake;
		data['duet_setting'] = this.duetSetting;
		data['nickname'] = this.nickname;
		data['shield_follow_notice'] = this.shieldFollowNotice;
		data['original_music_cover'] = this.originalMusicCover;
		data['follow_status'] = this.followStatus;
		data['ins_id'] = this.insId;
		data['unique_id_modify_time'] = this.uniqueIdModifyTime;
		data['is_phone_binded'] = this.isPhoneBinded;
		data['school_type'] = this.schoolType;
		data['status'] = this.status;
		data['twitter_name'] = this.twitterName;
		data['avatar_uri'] = this.avatarUri;
		data['signature'] = this.signature;
		data['weibo_verify'] = this.weiboVerify;
		data['unique_id'] = this.uniqueId;
//		data['geofencing'] = this.geofencing;
		return data;
	}
}

class AvatarLarger {
	String uri;
	List<String> urlList;

	AvatarLarger({this.uri, this.urlList});

	AvatarLarger.fromJson(Map<String, dynamic> json) {
		uri = json['uri'];
		urlList = json['url_list'].cast<String>();
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['uri'] = this.uri;
		data['url_list'] = this.urlList;
		return data;
	}
}

class AvatarThumb {
	String uri;
	List<String> urlList;

	AvatarThumb({this.uri, this.urlList});

	AvatarThumb.fromJson(Map<String, dynamic> json) {
		uri = json['uri'];
		urlList = json['url_list'].cast<String>();
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['uri'] = this.uri;
		data['url_list'] = this.urlList;
		return data;
	}
}

class VideoIcon {
	String uri;
	List<dynamic> urlList;

	VideoIcon({this.uri, this.urlList});

	VideoIcon.fromJson(Map<String, dynamic> json) {
		uri = json['uri'];
		urlList = json['url_list'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['uri'] = this.uri;
		data['url_list'] = this.urlList;
		return data;
	}
}

class Activity {
	int useMusicCount;
	int diggCount;

	Activity({this.useMusicCount, this.diggCount});

	Activity.fromJson(Map<String, dynamic> json) {
		useMusicCount = json['use_music_count'];
		diggCount = json['digg_count'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['use_music_count'] = this.useMusicCount;
		data['digg_count'] = this.diggCount;
		return data;
	}
}

class AvatarMedium {
	String uri;
	List<String> urlList;

	AvatarMedium({this.uri, this.urlList});

	AvatarMedium.fromJson(Map<String, dynamic> json) {
		uri = json['uri'];
		urlList = json['url_list'].cast<String>();
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['uri'] = this.uri;
		data['url_list'] = this.urlList;
		return data;
	}
}

class ShareInfo {
	String shareWeiboDesc;
	String shareTitle;
	String shareUrl;
	String shareDesc;

	ShareInfo({this.shareWeiboDesc, this.shareTitle, this.shareUrl, this.shareDesc});

	ShareInfo.fromJson(Map<String, dynamic> json) {
		shareWeiboDesc = json['share_weibo_desc'];
		shareTitle = json['share_title'];
		shareUrl = json['share_url'];
		shareDesc = json['share_desc'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['share_weibo_desc'] = this.shareWeiboDesc;
		data['share_title'] = this.shareTitle;
		data['share_url'] = this.shareUrl;
		data['share_desc'] = this.shareDesc;
		return data;
	}
}

class RiskInfos {
	bool warn;
	String content;
	bool riskSink;
	int type;

	RiskInfos({this.warn, this.content, this.riskSink, this.type});

	RiskInfos.fromJson(Map<String, dynamic> json) {
		warn = json['warn'];
		content = json['content'];
		riskSink = json['risk_sink'];
		type = json['type'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['warn'] = this.warn;
		data['content'] = this.content;
		data['risk_sink'] = this.riskSink;
		data['type'] = this.type;
		return data;
	}
}

class ChaList {
	int userCount;
	String cid;
	bool isPgcshow;
	String chaName;
	String schema;
	int type;
	int subType;
	String desc;

	ChaList({this.userCount, this.cid, this.isPgcshow, this.chaName, this.schema, this.type, this.subType, this.desc});

	ChaList.fromJson(Map<String, dynamic> json) {
		userCount = json['user_count'];
		cid = json['cid'];
		isPgcshow = json['is_pgcshow'];
		chaName = json['cha_name'];
		schema = json['schema'];
		type = json['type'];
		subType = json['sub_type'];
		desc = json['desc'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['user_count'] = this.userCount;
		data['cid'] = this.cid;
		data['is_pgcshow'] = this.isPgcshow;
		data['cha_name'] = this.chaName;
		data['schema'] = this.schema;
		data['type'] = this.type;
		data['sub_type'] = this.subType;
		data['desc'] = this.desc;
		return data;
	}
}

class Statistics {
	int playCount;
	String awemeId;
	int commentCount;
	int shareCount;
	int diggCount;

	Statistics({this.playCount, this.awemeId, this.commentCount, this.shareCount, this.diggCount});

	Statistics.fromJson(Map<String, dynamic> json) {
		playCount = json['play_count'];
		awemeId = json['aweme_id'];
		commentCount = json['comment_count'];
		shareCount = json['share_count'];
		diggCount = json['digg_count'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['play_count'] = this.playCount;
		data['aweme_id'] = this.awemeId;
		data['comment_count'] = this.commentCount;
		data['share_count'] = this.shareCount;
		data['digg_count'] = this.diggCount;
		return data;
	}
}

class Status {
	bool withGoods;
	bool isDelete;
	int privateStatus;
	bool withFusionGoods;
	bool allowComment;
	bool allowShare;
	bool isPrivate;

	Status({this.withGoods, this.isDelete, this.privateStatus, this.withFusionGoods, this.allowComment, this.allowShare, this.isPrivate});

	Status.fromJson(Map<String, dynamic> json) {
		withGoods = json['with_goods'];
		isDelete = json['is_delete'];
		privateStatus = json['private_status'];
		withFusionGoods = json['with_fusion_goods'];
		allowComment = json['allow_comment'];
		allowShare = json['allow_share'];
		isPrivate = json['is_private'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['with_goods'] = this.withGoods;
		data['is_delete'] = this.isDelete;
		data['private_status'] = this.privateStatus;
		data['with_fusion_goods'] = this.withFusionGoods;
		data['allow_comment'] = this.allowComment;
		data['allow_share'] = this.allowShare;
		data['is_private'] = this.isPrivate;
		return data;
	}
}
