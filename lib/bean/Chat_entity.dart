class ChatEntity {
	String picOriginal;
	String createTime;
	String msgContent;
	String picThumbnail;
	String picLarge;
	String msgType;
	String id;
	Visitor visitor;
	String picMedium;

	ChatEntity({this.picOriginal, this.createTime, this.msgContent, this.picThumbnail, this.picLarge, this.msgType, this.id, this.visitor, this.picMedium});

	ChatEntity.fromJson(Map<String, dynamic> json) {
		picOriginal = json['pic_original'].toString();
		DateTime ct = DateTime.fromMillisecondsSinceEpoch(int.parse(json['create_time']));
		createTime = ct.year.toString()+'-'+ct.month.toString()+'-'+ct.day.toString();

		msgContent = json['msg_content'];
		picThumbnail = json['pic_thumbnail'].toString();
		picLarge = json['pic_large'].toString();
		msgType = json['msg_type'];
		id = json['id'];
		visitor = json['visitor'] != null ? new Visitor.fromJson(json['visitor']) : null;
		picMedium = json['pic_medium'].toString();
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, String>();
		data['pic_original'] = this.picOriginal;
		data['create_time'] = this.createTime;
		data['msg_content'] = this.msgContent;
		data['pic_thumbnail'] = this.picThumbnail;
		data['pic_large'] = this.picLarge;
		data['msg_type'] = this.msgType;
		data['id'] = this.id;
		if (this.visitor != null) {
      data['visitor'] = this.visitor.toJson();
    }
		data['pic_medium'] = this.picMedium;
		return data;
	}
}

class Visitor {
	String uid;
	String udid;
	AvatarThumbnail avatarThumbnail;
	AvatarMedium avatarMedium;
	AvatarLarge avatarLarge;

	Visitor({this.uid, this.udid, this.avatarThumbnail, this.avatarMedium, this.avatarLarge});

	Visitor.fromJson(Map<String, dynamic> json) {
		uid = json['uid'];
		udid = json['udid'];
		avatarThumbnail = json['avatar_thumbnail'] != null ? new AvatarThumbnail.fromJson(json['avatar_thumbnail']) : null;
		avatarMedium = json['avatar_medium'] != null ? new AvatarMedium.fromJson(json['avatar_medium']) : null;
		avatarLarge = json['avatar_large'] != null ? new AvatarLarge.fromJson(json['avatar_large']) : null;
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['uid'] = this.uid;
		data['udid'] = this.udid;
		if (this.avatarThumbnail != null) {
      data['avatar_thumbnail'] = this.avatarThumbnail.toJson();
    }
		if (this.avatarMedium != null) {
      data['avatar_medium'] = this.avatarMedium.toJson();
    }
		if (this.avatarLarge != null) {
      data['avatar_large'] = this.avatarLarge.toJson();
    }
		return data;
	}
}

class AvatarThumbnail {
	String fileId;
	String url;
	int width;
	int height;
	String type;

	AvatarThumbnail({this.fileId, this.url, this.width, this.height, this.type});

	AvatarThumbnail.fromJson(Map<String, dynamic> json) {
		fileId = json['file_id'];
		url = json['url'];
		width = json['width'];
		height = json['height'];
		type = json['type'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['file_id'] = this.fileId;
		data['url'] = this.url;
		data['width'] = this.width;
		data['height'] = this.height;
		data['type'] = this.type;
		return data;
	}
}

class AvatarMedium {
	String fileId;
	String url;
	int width;
	int height;
	String type;

	AvatarMedium({this.fileId, this.url, this.width, this.height, this.type});

	AvatarMedium.fromJson(Map<String, dynamic> json) {
		fileId = json['file_id'];
		url = json['url'];
		width = json['width'];
		height = json['height'];
		type = json['type'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['file_id'] = this.fileId;
		data['url'] = this.url;
		data['width'] = this.width;
		data['height'] = this.height;
		data['type'] = this.type;
		return data;
	}
}

class AvatarLarge {
	String fileId;
	String url;
	int width;
	int height;
	String type;

	AvatarLarge({this.fileId, this.url, this.width, this.height, this.type});

	AvatarLarge.fromJson(Map<String, dynamic> json) {
		fileId = json['file_id'];
		url = json['url'];
		width = json['width'];
		height = json['height'];
		type = json['type'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['file_id'] = this.fileId;
		data['url'] = this.url;
		data['width'] = this.width;
		data['height'] = this.height;
		data['type'] = this.type;
		return data;
	}
}
