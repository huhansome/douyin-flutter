class UserEntity {
	String videoIconVirtualUri;
	String weiboUrl;
	bool acceptPrivatePolicy;
	bool preventDownload;
	int appleAccount;
	int shieldFollowNotice;
	int commerceUserLevel;
	int schoolType;
	int followStatus;
	bool hideLocation;
	String accountRegion;
	bool isBindedWeibo;
	String schoolPoiId;
	bool hasOrders;
	String twitterName;
	int constellation;
	String totalFavorited;
	String twitterId;
	int authorityStatus;
	int favoritingCount;
	int neiguangShield;
	bool isPhoneBinded;
	int liveVerify;
	int shieldCommentNotice;
	int liveAgreement;
	bool withCommerceEntry;
	String uniqueId;
	int createTime;
	bool userCanceled;
	String shortId;
	int followerStatus;
	String enterpriseVerifyReason;
	VideoIcon videoIcon;
	int followingCount;
	String shareQrcodeUri;
	String bindPhone;
	Null originalMusicQrcode;
	String region;
	List<Null> geofencing;
	int status;
	int roomId;
	String birthday;
	bool hideSearch;
	int liveAgreementTime;
	AvatarMedium avatarMedium;
	int gender;
	Activity activity;
	int duetSetting;
	String signature;
	bool storyOpen;
	Null originalMusicCover;
	bool isGovMediaVip;
	int secret;
	String weiboSchema;
	int followerCount;
	int storyCount;
	AvatarLarger avatarLarger;
	String uid;
	String googleAccount;
	int awemeCount;
	String nickname;
	int commentSetting;
	bool hasEmail;
	int uniqueIdModifyTime;
	String weiboVerify;
	bool isAdFake;
	String youtubeChannelId;
	String youtubeChannelTitle;
	AvatarThumb avatarThumb;
	String schoolName;
	int verificationType;
	bool isVerified;
	int specialLock;
	String verifyInfo;
	int userRate;
	int needRecommend;
	int reflowPageUid;
	int shieldDiggNotice;
	String customVerify;
	String weiboName;
	String avatarUri;
	String insId;
	int reflowPageGid;

	UserEntity({this.videoIconVirtualUri, this.weiboUrl, this.acceptPrivatePolicy, this.preventDownload, this.appleAccount, this.shieldFollowNotice, this.commerceUserLevel, this.schoolType, this.followStatus, this.hideLocation, this.accountRegion, this.isBindedWeibo, this.schoolPoiId, this.hasOrders, this.twitterName, this.constellation, this.totalFavorited, this.twitterId, this.authorityStatus, this.favoritingCount, this.neiguangShield, this.isPhoneBinded, this.liveVerify, this.shieldCommentNotice, this.liveAgreement, this.withCommerceEntry, this.uniqueId, this.createTime, this.userCanceled, this.shortId, this.followerStatus, this.enterpriseVerifyReason, this.videoIcon, this.followingCount, this.shareQrcodeUri, this.bindPhone, this.originalMusicQrcode, this.region, this.geofencing, this.status, this.roomId, this.birthday, this.hideSearch, this.liveAgreementTime, this.avatarMedium, this.gender, this.activity, this.duetSetting, this.signature, this.storyOpen, this.originalMusicCover, this.isGovMediaVip, this.secret, this.weiboSchema, this.followerCount, this.storyCount, this.avatarLarger, this.uid, this.googleAccount, this.awemeCount, this.nickname, this.commentSetting, this.hasEmail, this.uniqueIdModifyTime, this.weiboVerify, this.isAdFake, this.youtubeChannelId, this.youtubeChannelTitle, this.avatarThumb, this.schoolName, this.verificationType, this.isVerified, this.specialLock, this.verifyInfo, this.userRate, this.needRecommend, this.reflowPageUid, this.shieldDiggNotice, this.customVerify, this.weiboName, this.avatarUri, this.insId, this.reflowPageGid});

	UserEntity.fromJson(Map<String, dynamic> json) {
		videoIconVirtualUri = json['video_icon_virtual_URI'];
		weiboUrl = json['weibo_url'];
		acceptPrivatePolicy = json['accept_private_policy'];
		preventDownload = json['prevent_download'];
		appleAccount = json['apple_account'];
		shieldFollowNotice = json['shield_follow_notice'];
		commerceUserLevel = json['commerce_user_level'];
		schoolType = json['school_type'];
		followStatus = json['follow_status'];
		hideLocation = json['hide_location'];
		accountRegion = json['account_region'];
		isBindedWeibo = json['is_binded_weibo'];
		schoolPoiId = json['school_poi_id'];
		hasOrders = json['has_orders'];
		twitterName = json['twitter_name'];
		constellation = json['constellation'];
		totalFavorited = json['total_favorited'];
		twitterId = json['twitter_id'];
		authorityStatus = json['authority_status'];
		favoritingCount = json['favoriting_count'];
		neiguangShield = json['neiguang_shield'];
		isPhoneBinded = json['is_phone_binded'];
		liveVerify = json['live_verify'];
		shieldCommentNotice = json['shield_comment_notice'];
		liveAgreement = json['live_agreement'];
		withCommerceEntry = json['with_commerce_entry'];
		uniqueId = json['unique_id'];
		createTime = json['create_time'];
		userCanceled = json['user_canceled'];
		shortId = json['short_id'];
		followerStatus = json['follower_status'];
		enterpriseVerifyReason = json['enterprise_verify_reason'];
		videoIcon = json['video_icon'] != null ? new VideoIcon.fromJson(json['video_icon']) : null;
		followingCount = json['following_count'];
		shareQrcodeUri = json['share_qrcode_uri'];
		bindPhone = json['bind_phone'];
		originalMusicQrcode = json['original_music_qrcode'];
		region = json['region'];
		if (json['geofencing'] != null) {
			geofencing = new List<Null>();
			json['geofencing'].forEach((v) { geofencing.add(v); });
		}
		status = json['status'];
		roomId = json['room_id'];
		birthday = json['birthday'];
		hideSearch = json['hide_search'];
		liveAgreementTime = json['live_agreement_time'];
		avatarMedium = json['avatar_medium'] != null ? new AvatarMedium.fromJson(json['avatar_medium']) : null;
		gender = json['gender'];
		activity = json['activity'] != null ? new Activity.fromJson(json['activity']) : null;
		duetSetting = json['duet_setting'];
		signature = json['signature'];
		storyOpen = json['story_open'];
		originalMusicCover = json['original_music_cover'];
		isGovMediaVip = json['is_gov_media_vip'];
		secret = json['secret'];
		weiboSchema = json['weibo_schema'];
		followerCount = json['follower_count'];
		storyCount = json['story_count'];
		avatarLarger = json['avatar_larger'] != null ? new AvatarLarger.fromJson(json['avatar_larger']) : null;
		uid = json['uid'];
		googleAccount = json['google_account'];
		awemeCount = json['aweme_count'];
		nickname = json['nickname'];
		commentSetting = json['comment_setting'];
		hasEmail = json['has_email'];
		uniqueIdModifyTime = json['unique_id_modify_time'];
		weiboVerify = json['weibo_verify'];
		isAdFake = json['is_ad_fake'];
		youtubeChannelId = json['youtube_channel_id'];
		youtubeChannelTitle = json['youtube_channel_title'];
		avatarThumb = json['avatar_thumb'] != null ? new AvatarThumb.fromJson(json['avatar_thumb']) : null;
		schoolName = json['school_name'];
		verificationType = json['verification_type'];
		isVerified = json['is_verified'];
		specialLock = json['special_lock'];
		verifyInfo = json['verify_info'];
		userRate = json['user_rate'];
		needRecommend = json['need_recommend'];
		reflowPageUid = json['reflow_page_uid'];
		shieldDiggNotice = json['shield_digg_notice'];
		customVerify = json['custom_verify'];
		weiboName = json['weibo_name'];
		avatarUri = json['avatar_uri'];
		insId = json['ins_id'];
		reflowPageGid = json['reflow_page_gid'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['video_icon_virtual_URI'] = this.videoIconVirtualUri;
		data['weibo_url'] = this.weiboUrl;
		data['accept_private_policy'] = this.acceptPrivatePolicy;
		data['prevent_download'] = this.preventDownload;
		data['apple_account'] = this.appleAccount;
		data['shield_follow_notice'] = this.shieldFollowNotice;
		data['commerce_user_level'] = this.commerceUserLevel;
		data['school_type'] = this.schoolType;
		data['follow_status'] = this.followStatus;
		data['hide_location'] = this.hideLocation;
		data['account_region'] = this.accountRegion;
		data['is_binded_weibo'] = this.isBindedWeibo;
		data['school_poi_id'] = this.schoolPoiId;
		data['has_orders'] = this.hasOrders;
		data['twitter_name'] = this.twitterName;
		data['constellation'] = this.constellation;
		data['total_favorited'] = this.totalFavorited;
		data['twitter_id'] = this.twitterId;
		data['authority_status'] = this.authorityStatus;
		data['favoriting_count'] = this.favoritingCount;
		data['neiguang_shield'] = this.neiguangShield;
		data['is_phone_binded'] = this.isPhoneBinded;
		data['live_verify'] = this.liveVerify;
		data['shield_comment_notice'] = this.shieldCommentNotice;
		data['live_agreement'] = this.liveAgreement;
		data['with_commerce_entry'] = this.withCommerceEntry;
		data['unique_id'] = this.uniqueId;
		data['create_time'] = this.createTime;
		data['user_canceled'] = this.userCanceled;
		data['short_id'] = this.shortId;
		data['follower_status'] = this.followerStatus;
		data['enterprise_verify_reason'] = this.enterpriseVerifyReason;
		if (this.videoIcon != null) {
      data['video_icon'] = this.videoIcon.toJson();
    }
		data['following_count'] = this.followingCount;
		data['share_qrcode_uri'] = this.shareQrcodeUri;
		data['bind_phone'] = this.bindPhone;
		data['original_music_qrcode'] = this.originalMusicQrcode;
		data['region'] = this.region;
		if (this.geofencing != null) {
      data['geofencing'] = this.geofencing.map((v) => v).toList();
    }
		data['status'] = this.status;
		data['room_id'] = this.roomId;
		data['birthday'] = this.birthday;
		data['hide_search'] = this.hideSearch;
		data['live_agreement_time'] = this.liveAgreementTime;
		if (this.avatarMedium != null) {
      data['avatar_medium'] = this.avatarMedium.toJson();
    }
		data['gender'] = this.gender;
		if (this.activity != null) {
      data['activity'] = this.activity.toJson();
    }
		data['duet_setting'] = this.duetSetting;
		data['signature'] = this.signature;
		data['story_open'] = this.storyOpen;
		data['original_music_cover'] = this.originalMusicCover;
		data['is_gov_media_vip'] = this.isGovMediaVip;
		data['secret'] = this.secret;
		data['weibo_schema'] = this.weiboSchema;
		data['follower_count'] = this.followerCount;
		data['story_count'] = this.storyCount;
		if (this.avatarLarger != null) {
      data['avatar_larger'] = this.avatarLarger.toJson();
    }
		data['uid'] = this.uid;
		data['google_account'] = this.googleAccount;
		data['aweme_count'] = this.awemeCount;
		data['nickname'] = this.nickname;
		data['comment_setting'] = this.commentSetting;
		data['has_email'] = this.hasEmail;
		data['unique_id_modify_time'] = this.uniqueIdModifyTime;
		data['weibo_verify'] = this.weiboVerify;
		data['is_ad_fake'] = this.isAdFake;
		data['youtube_channel_id'] = this.youtubeChannelId;
		data['youtube_channel_title'] = this.youtubeChannelTitle;
		if (this.avatarThumb != null) {
      data['avatar_thumb'] = this.avatarThumb.toJson();
    }
		data['school_name'] = this.schoolName;
		data['verification_type'] = this.verificationType;
		data['is_verified'] = this.isVerified;
		data['special_lock'] = this.specialLock;
		data['verify_info'] = this.verifyInfo;
		data['user_rate'] = this.userRate;
		data['need_recommend'] = this.needRecommend;
		data['reflow_page_uid'] = this.reflowPageUid;
		data['shield_digg_notice'] = this.shieldDiggNotice;
		data['custom_verify'] = this.customVerify;
		data['weibo_name'] = this.weiboName;
		data['avatar_uri'] = this.avatarUri;
		data['ins_id'] = this.insId;
		data['reflow_page_gid'] = this.reflowPageGid;
		return data;
	}
}

class VideoIcon {
	String uri;
	List<Null> urlList;

	VideoIcon({this.uri, this.urlList});

	VideoIcon.fromJson(Map<String, dynamic> json) {
		uri = json['uri'];
		if (json['url_list'] != null) {
			urlList = new List<Null>();
			json['url_list'].forEach((v) { urlList.add(v); });
		}
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['uri'] = this.uri;
		if (this.urlList != null) {
      data['url_list'] = this.urlList.map((v) => v).toList();
    }
		return data;
	}
}

class AvatarMedium {
	String uri;
	List<String> urlList;

	AvatarMedium({this.uri, this.urlList});

	AvatarMedium.fromJson(Map<String, dynamic> json) {
		uri = json['uri'];
		urlList = json['url_list'].cast<String>();
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['uri'] = this.uri;
		data['url_list'] = this.urlList;
		return data;
	}
}

class Activity {
	int useMusicCount;
	int diggCount;

	Activity({this.useMusicCount, this.diggCount});

	Activity.fromJson(Map<String, dynamic> json) {
		useMusicCount = json['use_music_count'];
		diggCount = json['digg_count'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['use_music_count'] = this.useMusicCount;
		data['digg_count'] = this.diggCount;
		return data;
	}
}

class AvatarLarger {
	String uri;
	List<String> urlList;

	AvatarLarger({this.uri, this.urlList});

	AvatarLarger.fromJson(Map<String, dynamic> json) {
		uri = json['uri'];
		urlList = json['url_list'].cast<String>();
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['uri'] = this.uri;
		data['url_list'] = this.urlList;
		return data;
	}
}

class AvatarThumb {
	String uri;
	List<String> urlList;

	AvatarThumb({this.uri, this.urlList});

	AvatarThumb.fromJson(Map<String, dynamic> json) {
		uri = json['uri'];
		urlList = json['url_list'].cast<String>();
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['uri'] = this.uri;
		data['url_list'] = this.urlList;
		return data;
	}
}
