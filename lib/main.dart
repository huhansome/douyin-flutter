import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'util/Const.dart';
import 'package:tab_indicator_styler/tab_indicator_styler.dart';
import 'package:flutter/services.dart';
import 'pages/HomePage.dart';
import 'pages/MessagePage.dart';
import 'pages/NearPage.dart';
import 'pages/MePage.dart';
import 'package:myproject/util/ToastUtil.dart';

void main() {
  runApp(MyApp());
//  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Douyin',
//      theme: ThemeData(
//        // This is the theme of your application.
//        //
//        // Try running your application with "flutter run". You'll see the
//        // application has a blue toolbar. Then, without quitting the app, try
//        // changing the primarySwatch below to Colors.green and then invoke
//        // "hot reload" (press "r" in the console where you ran "flutter run",
//        // or simply save your changes to "hot reload" in a Flutter IDE).
//        // Notice that the counter didn't reset back to zero; the application
//        // is not restarted.
//        primarySwatch: Colors.black,
//      ),
      home: MyTabbarPage(title: 'Flutter Douyin'),
    );
  }
}

class MyTabbarPage extends StatefulWidget {
  MyTabbarPage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;
  var s =  Colors.yellow;
  @override
  _MyTabbarPageState createState() => _MyTabbarPageState();
}

class _MyTabbarPageState extends State<MyTabbarPage>
    with SingleTickerProviderStateMixin {
  var _currentIndex = 0;
  var selectedLabelStyle = TextStyle(fontSize: 15, color: Colors.white);
  var unselectedLabelStyle = TextStyle(fontSize: 16, color: Colors.white70);
//   final double bottomPadding = MediaQuery.of(context).padding.bottom;
  var pageController = PageController(
    initialPage: 0,
    viewportFraction: 1,
    keepPage: true,
  );

  void _onTap(index) {
    setState(() {
      if (index == 2) {
        ToastUtil.showInfo('发布视频');
        return;
      }

      _currentIndex = index;
      if (index > 2) {
        index -= 1;
      }
      pageController.jumpToPage(index);
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
//      appBar: AppBar(
//        title: Text(widget.title),
//        backgroundColor: Const.themeColor,
//      ),
//      backgroundColor: Const.themeColor,
      body: PageView(
        scrollDirection: Axis.horizontal,
        reverse: false,
        controller: pageController,
        physics: NeverScrollableScrollPhysics(),
        pageSnapping: true,
        children: <Widget>[
          HomePage(),
          NearPage(),
          MessagePage(),
          MePage(),
        ],
      ),
      bottomNavigationBar: MyTabbar(
        selectedLabelStyle: TextStyle(fontSize: 15, color: Colors.white),
        unselectedLabelStyle: TextStyle(fontSize: 16, color: Colors.white10),
        tabs: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                child: Text(
                  '首页',
                  style: _currentIndex == 0 ? selectedLabelStyle : unselectedLabelStyle,
                ),
              ),
              Container(
                color: _currentIndex == 0 ? Colors.white : Colors.transparent,
                margin: EdgeInsets.only(top: 10),
                child: Text(''),
                width: 20,
                height: 2,
              )
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                child: Text(
                  '附近',
                  style: _currentIndex == 1 ? selectedLabelStyle : unselectedLabelStyle,
                ),
              ),
              Container(
                color: _currentIndex == 1 ? Colors.white : Colors.transparent,
                margin: EdgeInsets.only(top: 10),
                child: Text(''),
                width: 20,
                height: 2,
              )
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Image.asset(Const.tabbarAssets + 'icon_home_add.png', width: 40, height: 40,)
//              Container(
//                child: Text(
//                  '发布',
//                  style: _currentIndex == 2 ? selectedLabelStyle : unselectedLabelStyle,
//                ),
//              ),
//              Container(
//                color: _currentIndex == 2 ? Colors.white : Colors.black87,
//                margin: EdgeInsets.only(top: 10),
//                child: Text(''),
//                width: 20,
//                height: 2,
//              )
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                child: Text(
                  '消息',
                  style: _currentIndex == 3 ? selectedLabelStyle : unselectedLabelStyle,
                ),
              ),
              Container(
                color: _currentIndex == 3 ? Colors.white : Colors.transparent,
                margin: EdgeInsets.only(top: 10),
                child: Text(''),
                width: 20,
                height: 2,
              )
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                child: Text(
                  '我',
                  style: _currentIndex == 4 ? selectedLabelStyle : unselectedLabelStyle,
                ),
              ),
              Container(
                color: _currentIndex == 4 ? Colors.white : Colors.transparent,
                margin: EdgeInsets.only(top: 10),
                child: Text(''),
                width: 20,
                height: 2,
              )
            ],
          ),
        ],
        onTap: _onTap,
      ),
    );
  }
}

class MyTabbar extends StatefulWidget {
  const MyTabbar({
    Key key,
    this.tabs,
    this.selectedLabelStyle,
    this.unselectedLabelStyle,
    this.enableFeedback,
    this.onTap,
  }) : super(key: key);

  final List<Widget> tabs;
  final TextStyle selectedLabelStyle;
  final TextStyle unselectedLabelStyle;
  final bool enableFeedback;
  final ValueChanged<int> onTap;

  @override
  _MyTabbarState createState() => _MyTabbarState();
}

class _MyTabbarState extends State<MyTabbar> {
  var _currentIndex = 0;
  void _onTap(index) {
    setState(() {
      if (widget.onTap != null) {
        widget.onTap(index);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final List<Widget> wrappedTabs = <Widget>[
      for (int i = 0; i < widget.tabs.length; i += 1)
        Container(
          height: 49,
          child: Padding(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: Container(
              height: 49,
              child: widget.tabs[i]
            ),
          ),
        ),
    ];

    final int tabCount = widget.tabs.length;
    for (int index = 0; index < tabCount; index += 1) {
      wrappedTabs[index] = InkWell(
        onTap: () {
          _onTap(index);
        },
//        splashColor: Colors.white70,
//        highlightColor: Colors.white70,
        enableFeedback: widget.enableFeedback ?? true,
        child: Stack(
          children: <Widget>[
            wrappedTabs[index],
          ],
        ),
      );
    }

    return Container(
      color: Colors.black87,
      height: 49 + MediaQuery.of(context).padding.bottom,
      child: Row(
        crossAxisAlignment:CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: wrappedTabs,
      ),
    );
  }
}
