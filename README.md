# douyin-flutter

抖音flutter版本，用flutter模仿的一个抖音客户端，安卓iOS双端支持，项目主要用于熟悉flutter技术，验证flutter技术的可行性，同时加深对于flutter的理解，对于新手学习flutter来说是一个很好的可参考的demo

## 功能
- [x] 个人中心界面
- [x] 主页
- [x] 视频播放界面
- [x] 同城（附近）界面
- [ ] 视频发布界面
- [x] 消息界面
- [ ] IM功能
- [ ] 直播界面
- [x] 编辑个人资料界面
- [ ] 搜索界面
- [ ] 数据来源改为正式抖音API接口
- [ ] 项目代码整理优化，界面美化

## 鸣谢
- [cached_network_image](https://github.com/Baseflow/flutter_cached_network_image)
- [fluttertoast](https://github.com/ponnamkarthik/FlutterToast)
- [flutter_easyrefresh](https://github.com/xuelongqy/flutter_easyrefresh)
- [fijkplayer](https://github.com/befovy/fijkplayer)
- [marquee](https://github.com/marcelgarus/marquee)
- 现目前使用的本地数据来源于[此项目](https://github.com/sshiqiao/douyin-ios-objectc)的本地数据
